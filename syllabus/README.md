# General outline of the course

This is the original plan of the course, which may be amended.


MOOCs outline: each week (or module) contains 4 blocks, 2 or which are optional. 

 * Transmission of knowledge: content directly taught (text, video, etc.)
 * Interaction: social part (forum, live Q&A, etc.)
 * Non-evaluated activity: exercices that have to be done in autonomy
 * Evaluation

In addition, a central idea of this MOOC is to lead the students and get them
to actually contribute code to a FLOSS project. The final evaluation of the
MOOC being that their commit should be accepted. The relevant parts are marked
in **bold** below.

For the social interactions, the interactions aren't meant to be only within the MOOC, they also include social interactions with the larger FOSS community, as well as some of the FOSS projects being contributed to. Contributions already require some degree of socialization by themselves, but beyond this, free software has a strong community foundation, so one of the goals is to mix participants with the rest of the community, from the start, and make participants feel part of the community of the project they want to contribute to from the very beginning. Interactions between students (about the MOOC and its contents) remains important too however, as it allows learners to both ask for clarifications and try to provide clarifications to others.


| Week | Title | Transmission | Interaction | Activities | Evaluation | Week type |
|:--|:--|:--|:--|:--|:--|:--|
| 0 | Introduction / About | Who are we, what is this MOOC, how to contribute? | | | | |
| 1 | What is Free Software? | Introduction, 4 freedoms, History, "Free software" vs "open source" | Introduce yourself, What’s your motivation to learn about free software ? | Find many examples of free software (try to find #users, #devs) | MCQ | Theory |
| 2 | Where? | Git, github, gitlab, Merge requests, Bug management (tech : weblinux?) | ? | try to fork a given project (specific one for the course (on a gitlab?)), try to clone it (locally) | online shell : evaluate git abilities | hands-on |
| 3 | Who? | Organizations, Licenses, Economic models, charities | possible debates on: - Comparison of licenses? - Open core model ? - Examples ? | find examples (organization, economic model, etc), **FIND A PROJECT** (in a programming language you know) | QCM | Theory |
| 4 | Co-ntributing | Community management, IRC/ML/etc, CoC, Diversity, designers/other types of contributors, History | Debate ? (dangerous), Live Q&A with invited people? | find out more about Codes of conduscts, **GET IN TOUCH (IRC) + find out about code/bugtracker/ml** | ? | |
| 5 | Advanced tooling | Testing, Code reviews, Code analysis, build (make/cmake/maven/docker/etc) | forum: discuss about the projects to avoid duplicates, Share tips about projects (which are friendlier, etc), Help one another on the project, live q&a about project? | Challenge : installing/test things, **REPRODUCE A MINOR BUG FROM THE BUGTRACKER** | online shell : build something, Write a test | practice |
| 6 | Diving in big codebases | IDEs, Git grep, and similar tools and tips | See above ^| **FIND THE SOURCE OF THE BUG (and think of a fix)** | find where stuff is in codebases in various languages | practice |
| 7 | Conclusion | Conclude | See above ^ | give feedback about the mooc | **COMMIT THE FIX** | Conclusion |


The more technical exercises will probably use an in-browser terminal emulator
like [jsLinux](https://bellard.org/jslinux/) or [weblinux](http://weblinux.remisharrock.fr/), already used for instance in [Linux MOOCs](https://www.edx.org/course/linux-basics-the-command-line-interface), and an idea 
for some of the theoretical parts is to try to get interviews of world experts 
in these topics (at [SFC](https://sfconservancy.org/), [FSF(E)](https://fsfe.org/index.fr.html), [OSI](https://opensource.org/), etc.)

A complementary approach, which could help with marketing, would be to involve high profile figures who would be known to all developers (not just within the FOSS community, to attract people outside of it). I.e. a bit the masterclass.com approach -- "get Linus to teach you contributions" - or people like Jeff Atwood.

This folder also contains [Resources](Resources.md) and
[Personas](Personas.md). 

Resources are a collection of related MOOCs, courses,
books or documentaries that may be of interest in the conception of the course.

The personas represent stereotypical profiles of people who could register to
the course, and keeping in mind those profiles when creating resources may be
great for staying focused on the goal of a MOOC.


