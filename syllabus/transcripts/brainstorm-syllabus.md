# Brainstorm - Syllabus (Meeting Transcript)

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1612978004171

S1: 00:00:00.000 All right.

S2: 00:00:02.341 All right. Welcome, everyone, and thanks for coming. So you probably know what this meeting is about, but. So it's about the general orientation of the MOOC, Contributing to FLOSS. So I've put in the notes-- no, in the pad the rough agenda that we want to follow tonight, and I suggest we start with some introductions. So I will start. My name is Marc Jeanmougin. I'm a research engineer at Télécom Paris. I wanted to make a MOOC on contribution to free software. Actually, I first wanted to make just a course at Télécom for contribution to free software for students, but it apparently was easier to create a MOOC for everyone on Earth than changing the timetables in the school. So I got approved to do an online course on the topic. And apart from research and teaching, I'm also a contributor to Inkscape, which is a free software project. Rémi?

S1: 00:01:31.813 You say "she" for Inkscape?

S2: 00:01:36.380 I'm a board member. I'm one of the main current developers. So no really hierarchical structure, apart from who spends the most time on the topics.

S1: 00:01:50.838 All right. So myself, I'm Rémi Sharrock, a colleague of Marc. I am assistant professor, researcher, lecturer at Télécom Paris, the research lab but also engineering school. And I am in the networking and computer science department. And I'm doing also research on distributed computing and massive education. I've created many MOOCs on the C language, on Linux, the command line in French and English, MOOCs on how to make MOOCs. Also, I'm creating new MOOCs on Python. And I am in the process of "dégooglisation," in French, so getting out of Google and taking care of my own data. So I am focusing now on how to take some open-source projects and use them to create my own server services at home. And with Marc, I'm really happy to start this project on how to contribute to open-source software because there is a lot to do. And I think there is many people [crosstalk]--

S2: 00:03:31.139 We should say "FLOSS," not "open-source."

S1: 00:03:34.317 FLOSS. Sorry, FLOSS. So it's Free/Libre and Open-Source projects. And of course, we will have to discuss a little bit, at the beginning of the MOOC, what's the difference between "open-source" and "FLOSS" and all the terms, definitions, and stuff. That will be the first part of the MOOC, I would say. But the whole structure is-- you can see the whole structure, maybe, if you click somewhere in your screen. But the thing is, we wanted to create this MOOC from the beginning so that it is open to everyone for contribution. Because it's a MOOC on contribution, so we wanted to make it open for contribution from the very beginning. And this is the very first meeting we have open to the public so that we can discuss the content of the MOOC and the structure of the MOOC. And then afterwards, we will have many other meetings to discuss more into details each part of the MOOC: what activities, what content, what ideas we have, everything. So we want to make everybody comfortable contributing, and we will discuss about how to contribute later in this meeting. And I am just very happy to start this project openly. And that's it. So next, if someone want to present itself.

S3: 00:05:14.963 I can, maybe.

S1: 00:05:16.504 Yes?

S3: 00:05:16.716 So I'm a student in computer science in Bordeaux. And this is my last year, so I am now doing an internship in Paris. I never contributed in FLOSS, but I'm really interested in the subject. So I'm here today kind of a future student of this MOOC.

S2: 00:05:50.642 Did you learn about it in the Framablog, or--?

S3: 00:05:53.653 Yeah. Yes.

S1: 00:05:56.486 All right. So what kind of studies in Bordeaux did you do? Because I was working at Bordeaux before at the LaBRI research lab.

S3: 00:06:08.099 Okay. I am at INSA de Mathématiques app.

S1: 00:06:12.075 And I was a teacher at INSA de Mathématiques app, so I know the school quite well. All right. Welcome.

S3: 00:06:21.591 Thank you.

S2: 00:06:25.406 All right.

S4: 00:06:25.886 Next [up is?] Jérémy. Is that okay?

S2: 00:06:27.819 [inaudible]?

S1: 00:06:28.711 No, no. That's fine. Go ahead, Jérémy.

S4: 00:06:32.194 I'm Jérémy. I work in Bordeaux to Scalian society to web design-- oh, sorry, I have a echo on another computer. So I build their website infrastructure. I design [inaudible]. I'm [inaudible]. I develop to Docker, and I teach Git to my colleagues. That's why I am interesting to Git-- FLOSS, sorry, and MOOC.

S1: 00:07:33.927 All right. Thank you, Jérémy. Then we have Charlène, maybe? I don't know if you have a microphone, Charlène. Or maybe you can just put a sentence, if you don't want to speak, to present yourself. "Looking for the microphone." All right. Oh, I think she's doing an echo test.

S5: 00:08:06.242 Hello. Yeah. [inaudible]. So I'm Charlène. I [inaudible] digital learning. And I'm interested in FLOSS, so I'm [inaudible] contributor [inaudible]--

S6: 00:08:21.587 Charlène, it's a bit hard to hear you. Maybe you could speak closer to the microphone, or maybe there's something in the way. I'm not sure.

S5: 00:08:29.587 Can you hear me better?

S6: 00:08:31.594 Yeah, that's better.

S5: 00:08:33.324 Okay. So I'm Charlène. I'm from Bordeaux too. So yeah, I'm from Bordeaux, and I'm living with Jérémy Babin. He is my boyfriend. I'm not at all an IT person. But I'm interested in digital learning, and I'm following the Framablog, so that's why I'm here: because I wanted to check if I can contribute or not, even if I'm not an IT person. That's it.

S2: 00:09:04.480 Great.

S1: 00:09:05.372 Nice [crosstalk].

S2: 00:09:05.579 Yes, there will be definitely opportunities for non-IT people to contribute to the MOOC, or I hope so. Because, well, in the vision that's in my head of the future of the MOOC, there is a large part of the MOOC on the social aspects of contributions and not so much-- well, not that much on the real practical hands-on stuff. Xavier, maybe?

S6: 00:09:41.619 All right. So yeah, I'm Xavier Antoviaque. Well, I've been in the FLOSS community for quite a while, actually, maybe something like 15, probably almost 20 years now. So nowadays I run a company called OpenCraft, and we work mostly with the Open edX project, which is, interestingly enough, the platform that's going to be used for running this MOOC. So that was a good coincidence because within the Open edX community, we were looking at finding ways to onboard our contributors a bit better than we were doing until now. And one of those courses that we've identified is one about upstreaming, becoming a contributor, etc. And that's actually an interest of mine for quite a long time. I've worked on some previous initiatives around that, a little bit around Upstream University, which was another format, not really the online MOOC format, but more in person to train for contributions. And I mean, I've been involved in a bunch of different projects. I've been a board member at April, these kind of things. But I think for this course, it was really a nice coincidence to have found that roughly at the time we were starting to look at developing it. So rather than doing things in our own corner in good, free software spirit, the idea is to try to join forces and maybe work on how to do something together. So yeah, that's why I'm interested in this.
[silence]

S1: 00:11:41.171 Marc, [au micro?]? You speak without the microphone.

S2: 00:11:47.014 Sorry. Okay. I think everyone had a chance to introduce themselves here, if I'm not mistaken. So I will maybe talk a bit about the general idea of the MOOC. So as I said previously, the original idea before creating the MOOC was to make a course for students in our school, which are IT students. And I was already having this idea from similar courses that exists in other universities. For instance, I have a friend in Paris 8 University, who is called Pablo Rauzy. And he has a course for bachelor's students to make them do a contribution to an open-source project, and that's the subject of his course. So I know that it's not exactly the same to do a MOOC and to do a course in person with students that you see every week. But that was the basic idea of the course: take people who know how to program in some language and teach them the skills to basically know what open-source is, to know how to contact people in open-source projects, to have them really make the effort to contact open-source projects and go find where they store bugs - they obviously have bugs because every project has lots of bugs, simple or complicated - and do something for that project by interacting with them, so having a real contribution made to a project.

S2: 00:13:37.369 So in the general outline of the course, I wanted to have a structure too that ends with a contribution to a project. So I tried to take some inspiration from some courses, so this course and another course from Paris 7, I think, which does not exist under that name, but which is also about contributing to open-source. And I tried to identify which areas people who know how to program would need, basically, information and what we could teach them and what they need to know to contribute to a open-source project. And that's what I tried to put in the outline of the course.

S2: 00:14:33.756 So first thing is-- well, I did not do this structure of the MOOC in one go. So the end was set from the start, like commit a fix, basically. But from that, you need some skills. Like, the most technical skill you need and that's not really taught at school and it's very, very hard for people in IT that did not have either training or onboarding in a company or in an organization to do, is to basically navigate in big codebase because if you learn how to program, you basically learn how to program small programs, programs where you know where everything is, where you thought about everything yourself, and where you basically know roughly where things are. When you enter a big codebase, you don't know anything and you have no idea how it works, so you have to learn things, like, "Do not try to understand everything," "Try to understand exactly the things that interest your bug," "Try to see the documentation and to understand more the documentation than the code because that will help you more in the long term if the documentation exists and is up-to-date," and stuff like that. So that was basically a prerequisite that I tried to identify. And of course, all the social skills that people might not have. So knowing about free software is, in my opinion, sort of important to interact with free software communities so that you don't ask to see the manager or-- that's something that we already saw in some IRC channels, random people coming and asking, "Who is responsible for this software?" It's a community, so we might not have the same terms and cultures as what you might be used to. So you need to talk about, basically, the economic model, the licenses, the organizations, etc., to sort of draft a picture of the environment that you will step in by contributing to free software. And also, we need to talk about, basically, how to get in contact. So what is IRC, how mailing lists works, how community management works in different free software examples, etc., etc. So basically, the idea is that the five last weeks of the course-- I say "weeks," but it's more "modules" because they can take more than a week or less than a week. And usually in MOOCs, you can-- well, in many MOOCs, it's self-paced, which means you take as much time as you need to complete a module. Rémi can stop me if I'm wrong. But the idea is that at some point you will need to find a project that you can contribute to. You will need to get in touch with people in that project. You will need to find the bug trackers. You will need to know how to build their code. You will need how to reproduce a bug they have to, basically, assess yourself what this is about. And you will need to find the source of the bug in their codebase and to fix it.

S2: 00:18:34.976 And all of these steps in-- all these steps are steps that-- the only step that people who know how to program and just know how to program, know how to do is, basically, fix the bug. The rest is stuff we have to teach, and we have to teach them, basically, in that order because other orders do not really make sense. You have to talk to people, and you have to find where they hide their bugs, and then you have to help them. And I also put the first three weeks on general-- sorry, the first two weeks on general stuff. So what is free software? Basically, "Where are you stepping in?" which, in my view, is sort of a prerequisite. And of course, a week on Git, which is basically how you will interact with people on the technical side. Git is the platform for contributing, with GitLab and GitHub, mostly. So that's how I thought about the separations of modules in the course. It's probably not perfect, but it's the idea I had. So it makes for a quite long MOOC by MOOC standards since I expect that most of these six or seven modules can take several hours, and six modules or seven modules is already long for MOOC standards. Well, that's what's Rémi told me, at least. But I did not see areas where I could remove stuff. Like, I can remove "What is free software?" But then, I'm sending people to discuss with others without knowing anything about their culture, and I don't think that's a good thing to do. Or I could basically tell them, "Do stuff, and then do not know anything about Git so that you cannot do anything." And so this was for me the core stuff that I could not really remove from. I'm, obviously, open to comments and suggestions about this, and I think it's time for me to stop talking and to start listening.
[silence]

S1: 00:21:22.372 So all right. So for the first point, "Why this MOOC? The goal, the audience, and rough contents," I think we have everything here. Maybe we should put the link of the GitLab repository on the chat or somewhere so that you can go - okay - and see. So the thing is, we just started the project, and we are in the process of creating the structure, and then we will create the content of each module, etc. And we make this kind of brainstorm openly to the public with many people so that we can get feedback. So the idea here is to discuss about the main goals of the course, the structure of the course, maybe if you have some ideas of anything that is already existing, of courses that you already took, or ideas on what we should do inside the MOOC, then it is time for you to say it loud. And we want to have some feedback and ideas on what we should do because we just start the project and we have a rough idea, and we would like to have more ideas from people, like a big brainstorming. So just feel free to ask questions about this structure, about what we should put, what you would like to learn in this kind of MOOC. I don't know. Any idea is welcome.

S3: 00:23:31.751 Yes. So I would like to see in the MOOC how to find a project, and find a project that accepts contribution, especially, because that's something I struggle on right now.

S1: 00:23:57.380 That's interesting. So we have put that in the part three. As you can see in the structure, we have the introduction, "What is free software?" "Where?" - so GitHub, GitLab, etc. - and "Who?" And "Who?" we will try to have a list of organizations, licenses, economic models, charities. But in the organizations, maybe it's a first way of finding open-source projects. Some of the organizations are experts on giving a repertoire of all the open-source projects. Like, I'm thinking about Framasoft. Framasoft is a website with a lot of resources and a lot of pointers projects. But here, indeed, in the activity, as you can see, the main activity will be, "Find a project in a programming language you know, and maybe find examples of organizations, of economic models for open-source-- for FLOSS projects," sorry. But yes, indeed, we intend to put that activity in the week number three, as you can see. And maybe we will evaluate this in maybe multiple-choice questions and ask, "What project did you choose? For what reason? How did you find it?" etc., so. But indeed, when we will talk about this number three module, we will have to make sure that we have enough pointers to Bootstrap the search of a FLOSS project. So that's a good point.

S6: 00:26:11.879 Yeah. And I think on that point, especially coming from the Open edX community, I think it's good that the MOOC is opening up to any project, etc. because, well, I think a lot of people will come with maybe a project in mind, etc. But I also see how that can be tricky at times, when you want to do a contribution, to find the right one. So because we have, I think, some project that are interested, even, in participating with that, there could be at least maybe a partnership with some projects, where, if someone doesn't know or doesn't want to try multiple projects, etc., there would be kind of a safe space with those projects that want to participate, to know that there will be some reviewers around, or we'll be a little bit more lenient with the contributors because knowing that it's kind of the first contribution, etc. So I mean, this is not something that has been really decided on the Open edX project, but I think there would probably be an opportunity for that and plenty of people would be happy to help there. So maybe that could be a way.

S2: 00:27:27.583 Yeah. I know that some projects have better onboarding processes than others, and it would be good to have a list of projects that are known to be easy to contribute to, basically. There are, actually, already lists of lists of such projects, mostly centered on stuff like Good First Issue tag, Ungitable, equivalent stuff for open-source platforms.

S1: 00:27:58.655 Sometimes when I search for-- let me give you an example. One day I wanted to have a server for hosting podcasts for a friend. So of course, I wanted the project to be open-source and be able to self-host the server. So what I did is, I went to GitHub, and there is a kind of methodology to search a little bit. So you have to put keywords. There is a research engine. So of course, I put "podcasts" as a keyword. And then you can filter by language, you can filter by the popularity of the project, the activity of the project, if it is maintained; the community, if it is good-- or if it is-- I mean, not good, but big or not; if it is well maintained; if the community is reactive enough so that it is kind of a-- yeah, I'm trying to find the maturity of the project, somehow. But it's always tricky to find the right spot, the right project to work on. And even finding a project is very long. It's a long process sometimes. If you want to have something very specific, maybe it doesn't exist and you have to glue two projects together, etc., so. But there is a kind of methodology to search on this kind of platform, GitHub or GitLab. Maybe we can also mention that kind of process or give ideas on how to find projects like this.

S6: 00:29:51.657 Yep. And actually, on this topic, that brings up another related remark that I had that I made to Marc on the merge request is that finding the project and working towards that contribution is the longest thing that we'll have to do in that course. And I mean, that might be, I'm trying too much the details for that point. But I think there could be value in trying to start that process earlier, maybe from the very beginning. Obviously, when you arrive, you don't know anything about license or free software, so we would have to be careful about the pacing of that, but. And that could be also giving something concrete more quickly, like, "Okay, I want to do a contribution to that project," so starting to talk a bit with people, etc. That might be also something that keeps people interested in the first few weeks to [until they get there?].

S1: 00:30:49.135 Interesting. So maybe this activity about finding projects, even if it is a light activity, very simple at the beginning, we could start it earlier in the course. Maybe--

S4: 00:31:05.539 I think what we could do is, basically, tell about these bold steps as early as at the beginning of the course, like, "You will have to do that. So think about it, try to do your research, try to see what language you know, and try to see what projects that can interest you are in those languages." And even from the beginning of the course, saying up front, "This is the goal. This takes time and is not as easy as you can hope. Try to think about it even before getting to a point where you will have all the knowledge to do it." And maybe link to resources about lists of lists of projects early and repeatedly until that point.

S6: 00:32:11.429 That makes sense, especially because there might also be the interaction between the students, where people could be like, "Okay. I found that project, and it's interesting," or "Oh, this project doesn't reply to me. What do I do?" And those are the things, I think, that sounds like a good approach.
[silence]

S1: 00:32:37.173 All right. So thank you, Jérémy, for raising the point. And what about the others? Do you have ideas of, already, projects you would like to contribute to or--? I don't know.

S3: 00:32:57.002 I try to contribute to catch-up projects that was written in Python. So I know Python a lot, so I was happy that-- I feel confident about this project. However, I never received an answer to the issue I posted, so I'm just waiting now.

S1: 00:33:25.065 All right. So your contribution is, at least, to post an issue that you have with the software, right?

S3: 00:33:36.444 Yeah. I asked if the project was still maintained because the last commit was eight months ago or something like that.

S1: 00:33:47.075 Okay.

S6: 00:33:49.086 Do they have other discussion spaces like IRC or mailing lists or--?

S3: 00:33:54.003 I don't know. The project is only maintained by one person. So maybe I can find other social media that this person is using to contact them.

S1: 00:34:10.842 That's interesting because you say how many maintainers, when was the last activity on the project. These kind of question-- we have to ask these questions. When you have to find a project, you have to ask yourself many questions. And so yeah, that's everything will be in the "Who?" Marc?

S2: 00:34:39.112 It's a long week.

S1: 00:34:41.180 It's also, I can see, in number four, Community, how to get in touch with the community and the maintainers.

S2: 00:34:53.681 Yeah. But if you see that the organization around the project is just one person, then you know that either you will be able to contact that person or not. And if not, then there's no need to continue.

S1: 00:35:10.889 Okay. So in a sense, that excludes small projects.

S2: 00:35:18.026 No. If you have a reactive maintainer, then it's not a bad thing. It's just that he won't be able to withstand 20 students coming to him with ideas how to improve the project because he's alone. But it's still possible to have one or two people going to that project. If he answers and he can review contributions, then there is no problem with a single-maintainer project.

S1: 00:35:51.458 All right. So if it is a small project, the reactivity of the one or two maintainers, maybe three, is also kind of important, right?

S2: 00:36:03.696 Even for big projects. If you go to Mozilla with a suggestion, and after eight months, you don't have an answer, then what's the point?

S1: 00:36:13.484 I remember [crosstalk] from Firefox waiting for 12 years or something like that. Still, the issue is here and it's active, but.

S6: 00:36:28.666 Yeah. And I think that list of criteria is to use to pick the projects, like the ones that we've mentioned. But reactivity is also something which has different components, right? It's like, "Do requests get merged? Do people review within a few days or not?" these kind of things. So definitely, yeah, that's quite an important one. And actually, those ones can also be related to other things. It's that sometimes you have a project where people are reactive, you come, you post your pull request and nothing happens. Why? Because that's actually in an area where nobody is interested or they don't know you, you haven't talked to the right people. So it's really tricky, that part, to untangle.

S1: 00:37:12.688 Yeah. I was thinking also on fork of projects. Sometimes even big projects are forking and doing a new. Like, I don't know, an example is ownCloud becoming Nextcloud, and the community is progressively migrating from one project to another. And this kind of community migration, maybe you will have to detect it. If you go to a project very quickly, you have to know if it is still alive or where is the big community, if it is a big community migrating somewhere.

S2: 00:37:58.359 ownCloud still exists.

S1: 00:38:00.687 Right. But then how do you compare ownCloud and Nextcloud, for example? Well, I don't know if it is part of the whole MOOC. But the thing is, forking is also part of the game. Sometimes people don't agree about something, and they just want to make a fork because they don't agree on a certain point. And to know that it is possible to fork and maybe sometimes the community will also fork and you will have two communities - and this happens many times - maybe we will have to say that somewhere. But I don't know where, but.

S2: 00:38:57.077 I think it mostly happens around issues of either governance or license or economic credits with probably all of them linked together, basically.

S1: 00:39:09.709 Or maybe if you find projects, it is also good to ask yourselves about the history of the project, what it is based on, if it is a fork of a big project already existing, the context of the project.

S6: 00:39:27.954 And I think, yeah, that the fork topic is kind of a big topic because there is the type of fork that we are discussing here, which is a big community fork, creating a second maintained project. But what I've noticed a lot, especially when companies are involved into development, is that there is often a fix that will be done that will just remain on the fork. And often the company is thinking very short-term and being like, "Okay, but why would I spend time contributing that, where we can just have our own fork and that's it?" But there is a cost to that, of course, in terms of, "What do you do when there is the next upgrade coming?" and "Do you get other developers to care about your own code or your own use case?" etc. And in some way, that's related to those big community shifts when we have a real fork. It's like, "Do we decide to work together and find a middle ground with things and make it for others? Or do we just do something in our own corner, but then we pay the full cost of maintenance?" And I think every contributor ends up having to do that decision whenever we decide to go contributing, whenever there is a reviewer coming back and saying, "Yeah, I don't like your code there." Okay. Maybe he's right. Maybe he's wrong. But behind that is, there is the, "Do we work together? Do we try to find a middle ground?" And that's often really at the core of upstreaming, I think.
[silence]

S1: 00:41:13.861 So I think maybe something missing is-- so find a project, get in touch with the community. But something in between is not only finding the project, but also knowing more about the project. [crosstalk]--

S2: 00:41:29.152 Researching about it.

S1: 00:41:31.248 Sorry?

S2: 00:41:32.184 Researching information about it, basically.

S1: 00:41:34.243 Right. Yes. Something like that is missing, I think.

S2: 00:41:37.049 Yeah. It could be part of "find a project." It's not just "find a project." It's "find a project that you can contribute to and that will accept your contribution."

S1: 00:41:49.580 Right. Because [Polyèdre?] also was saying, "If the project is organized to accept contribution, subtests, some continuous integration, maybe we'll have to define a little bit CI, etc."

S2: 00:42:06.646 Yeah. I took some notes on the pad. Yeah. Another topic for-- we have 40 minutes left, so we have two or three items left. So the two items on the agenda that we have left is, "How to contribute to the MOOC?" and "What are the technical choices made or to be made for the MOOC?" So I suggest we start with how to contribute to the MOOC. Like, how do we welcome contributors, and how we share what we want to do or what we do?

S1: 00:42:57.991 Yes. [crosstalk].

S2: 00:42:59.280 And that's an open question.

S1: 00:43:03.444 Open to--

S2: 00:43:03.444 So the rough first idea was to do brainstorming sessions for every week that we did. We discussed the course outline, and I didn't hear any big criticism of the general structure, like the stuff that could happen before others, except the "find a project." That could be earlier. But the general idea of our outline looks fine mostly, so we will continue working in that frame. So the idea was to have a few brainstorming sessions, one per week from one to six, basically, with ideas on what exactly should we talk about and under which format, like videos or texts or links or documents or any other formats that we could think of because we are not really constrained in that area. And then once we have this outline of each week, then my idea was to tell that "If you're interested in doing one of those parts, then you can just take on the task. And if it's writing a page on a topic that should take someone to learn like 20 minutes, then you can do it. And then you can be reviewed and make a merge request and have people read it and make comments and continue with that sort of idea." But I'm open to other methods of organization. I mean, it's the first time I do that, so I don't have any experience at coordinating people to do a MOOC.

S6: 00:45:20.314 No, that sounds good to me. And I think, yeah, assigning specific parts to specific people is probably a good way to make sure stuff happens. What's the timeline for creating the MOOC? Because I know some people who might be interested in doing some of those contributions, but some of them just come back end of February. So yeah, just to get an idea of what you have in mind.

S2: 00:45:46.825 So the timeline of the brainstorming sessions is all of them in February, except one the last week in March. We already planned all those meetings two per week, basically, in February. They are all listed in the MOOC Issues on the GitLab with dates. And yeah, the idea would be that, when the brainstorming session ends, its results are posted, and then it's open to people who want to contribute assets or texts or stuff to these weeks. So people could come in March and say, "Oh, I saw that, in the third week, you want to speak about the GPL versus MIT because it warrants a whole page of 10 minutes' read. I can write a 10-minute text about GPL versus MIT, and I will do that."

S6: 00:47:02.237 Okay. So the brainstorming happens now, but the actual writing, producing of content, would be after the brainstorming sessions?

S2: 00:47:09.924 Yes. Yeah, yeah. No writing would happen in the brainstorming, except of the outline.

S6: 00:47:17.337 All right. Cool. And then--

S2: 00:47:17.881 Because one hour and a half is not enough to make more than an outline, and I'm not sure it's enough to make an outline with times.

S6: 00:47:27.850 For sure. And what about the completion of the course? Do you know when it will run the first time?

S2: 00:47:37.370 Ideally, in 2021-- the end of 2021.

S6: 00:47:42.749 Okay. Yeah, that's true. I remember you had mentioned that.

S2: 00:47:44.273 It's short, but I think it's possible at least for the first run, and then to iterate on that. But Rémi has more experience on that than me.

S1: 00:48:01.361 Yeah. Because I created so many MOOCs, and we'll see how it goes and how fast we create the first content. But a very good question is, [Polyèdre?] was asking if the contributions to the MOOC will be open after these six weeks. So what I understand is that contributions will be open-- well, the thing is, we want the MOOC to be open to contribution even during the execution of the MOOC and even after. So it will be a whole open project, open to contributions. Even if we migrate to another platform one day - we never know - still, we will have this base, the source - and we will have to discuss about the source - somewhere in a Git so that everybody can still continue to contribute to the project.

S2: 00:49:04.834 And given the CC BY-SA license, if someone doesn't like the direction that we take, he can still take contents and just give attribution, and make his MOOC or his course or his documents.

S1: 00:49:19.435 Make a fork.

S2: 00:49:22.943 Yeah. Well, ideally, they would contribute to the original project and be happier with a common project. But if it doesn't fit some purpose, then--

S1: 00:49:36.038 So another thing we were thinking, technically, is that the content in the course will be kind of editable. Like, if you want to make feedback content or modify any content of the course, we would like to have the very easy way for everybody to just click a link somewhere, and the link will redirect you directly to the editor of this content. And maybe we can talk a little bit about what is the format of the source of the course because we started this repository with a very basic format, Markdown. So everything here is in Markdown, and the platform we'll be using, edX-- the MOOC platform we'll be using will be edX. And edX has also other formats, like open learning XML, also a specific Markdown language to specify, for example, multiple-choice questions. And they have editors, like What You See Is What You Get editors that generate open XML, etc. So we are not sure now what source we should keep in the repository. Should we have very general sources, like Markdown, HTML, very generic things? Or should we have sources more specific to the platform we'll be using, like edX, for example? So we have to keep in mind that, if we want to either migrate or copy the MOOC in another platform, Moodle internally or any other platform, we have to make sure that the source code is generic enough so that it is easy to migrate to other platforms in the future, even if it is a long-term future. And we have to make sure [crosstalk]--

S2: 00:51:57.191 No. It's not just that. It's also to facilitate reusability of the contents in general.

S1: 00:52:02.237 Right. Because if some parts of the MOOC are great for an engineering school and one teacher want to use this specific parts into his course, maybe he wants to use the internal platform Moodle. And for him, it will be easy-- and she, it will be easy just to import if the source is generic enough so that it is easy to migrate to another platform. So we have to keep in mind this. Maybe we can give tools to be compatible with multiple platforms. We don't know how to do it, actually. Maybe, Xavier, you have some ideas also.

S6: 00:52:46.070 Yeah. I actually commented on the merge request because it's true that it's nice to have something that is multiplatform, just like a software that runs on different OS, etc. But it's definitely an additional layer. And also, you get drawbacks in terms of just being able to edit and produce the course itself today, right, because even if you want to just do a basic MCQ, you have a specific syntax in all of those platforms, so. And if you want something generic, does that mean that we define an additional syntax that then we convert into the other ones, or we pick one of the syntax and we convert that? I think it's a tricky one. My tendency - but it's just my opinion - is that, at least to start, it's good to keep things simple and just produce things already that will be good for Open edX. So it's not like if it uses a [proprietized?] format, anyway. It's a standard XML that is documented, etc. And at least, you can fully use all the tools and exercise formats, etc., that you have in Open edX. And then you could always imagine later on to create something that would translate into other formats. That's possible. But I think in the worst case, because you are pretty much, I guess, HTML, XML, the work to copy it or translate it somewhere else would not be the end of the world either. But that's me, and obviously, I'm much more used to Open edX, so I probably have a very biased opinion there. But yeah, that's at least how I would do it.

S2: 00:54:38.493 All right. Well, Charlène, if you're a digital learning engineer, maybe you have ideas about that?
[silence]

S5: 00:55:03.975 I'm willing to be a digital learning engineer.

S1: 00:55:08.911 Okay. So for you, that's an interesting question because, in my experience, I created a lot of content on edX, for example, and I know that it's not really easy to export and import content from/to edX to Moodle, for example, or to Coursera or to any other platform. There are, of course, protocols, like LTI protocol. And LTI is, indeed, somehow widely spread because it is compatible with Moodle, with edX, with Coursera, even if Coursera, you have to click and it opens a new window. But anyway, it works. And so what I did with my MOOCs is that I created all the activities with my own format and integrated everything with LTI. But the thing is, I don't use a generic format. I use a protocol - LTI is a protocol - to communicate with my servers, and the format I use in my servers is something that we created. It's ad hoc. It's not an open format. So I think a better way to do that is to have an open format somehow that is well known, well documented, with a good specification so that it is easy to-- not easy, but at least possible to do translator or something kind of to translate the source code into another one. But also, I like to use open protocols, like LTI, so that it is easy to integrate the contents because it's compatible, widely used. All right. So I think two things: formats of the source and integration protocols. These are two separate things, and we have to make sure that we are generic enough, I would say.

S6: 00:57:40.603 Yeah. For the format from Open edX, I've put two links. If you want the format itself, the second one would be the main one. So they have tried also to come up with something that is a standard. The thing is that a standard is only whoever decide to call it a standard, right? So you have a bunch of competing formats in a way. But at least there is this intention to have it defined and standard, so that is possible.
[silence]

S1: 00:58:23.395 And of course, we want every tool and every format open-, not closed-source. And of course, all the tools to create the content, at the best, would be FLOSS. And as you can see, we used BigBlueButton today. It's a FLOSS project.

S6: 00:58:51.468 In my opinion, for all the non-purely-textual formats, it might be good to use OLX. I don't know.

S1: 00:59:03.380 Open learning XML?

S2: 00:59:05.566 At least it gives you a structure to create quizzes, anything that is not just textual information.

S6: 00:59:22.579 Yeah. And it's true that for the text content, you could still keep the approach of having it in Markdown and just translating it into the XML format that's [pure?], like HTML in the component in OLX. So I think that that would be relatively easy.

S2: 00:59:40.249 But you probably have code in the Open edX project to convert?

S6: 00:59:48.269 So to convert between what? Between Markdown and HTML, or--?

S2: 00:59:53.033 Yeah. Well, Markdown and whatever format works for--

S6: 00:59:59.118 So I don't think there is something that comes from Markdown because the way it's done in Open edX is that, for everything that's text, it's HTML which is edited with an HTML editor. I think it's CKEditor, something like that. So there is nothing, at least that I know of currently, that uses Markdown. But at the same time, it's very pluggable, so you could definitely have an alternative text component that use Markdown as the source that could even allow you to edit the Markdown in Studio, etc., or use it directly from the source have the translation because, what is edited in Studio by the instructor is then translated into the HTML that is seen on the other side. So the mechanisms are there for that.

S2: 01:00:59.377 Okay. Yeah.
[silence]

S2: 01:01:17.995 Yeah. Yeah. The main other nontechnical question is about the planned length of the course in general. So maybe I will wait for Rémi to come back.

S1: 01:01:39.324 I'm back!

S2: 01:01:41.174 So I said that the main nontechnical question remaining was about the global length of the course and whether it would be a problem or not. Because, as I said, six or seven weeks of a few hours might be a lot, but I don't know if it's desirable or if it's possible to get done.

S1: 01:02:13.944 What I did for-- because I had a very big, big, big MOOC on C language and Linux command line, I divided it into seven small MOOCs, and I created an edX, what is called Professional Certificate. So if you finish the seven MOOCs, then you have a kind of certificate that says you are good to go for C programming and Linux in a enterprise somewhere. But--

S6: 01:02:49.660 Yeah. I mean, it's true that there are some sections for which it could be quite appropriate. For example, GitLab, maybe a lot of people will be familiar with that from their own projects and maybe even use already merge request. They are just not used to contribute to someone else's project. So that could always be a separate module or at least an optional week or something within the course. And I also remember that we talked about the persona, like about that manager who just want to figure out what is free software, etc. He won't be interested into the actual contribution, but he would be interested in that. So yeah, I did something around splitting or making things optional.

S2: 01:03:35.748 Yeah. Making things optional. Because normally, in a MOOC, if you don't finish the section, you cannot go to the next one, or--?

S1: 01:03:43.431 No. Usually, it's open.

S6: 01:03:48.322 Yeah. You can limit, but most MOOCs would leave everything open, yeah.

S2: 01:03:53.867 Okay. So in practice, if someone wants to skip parts, they can?

S1: 01:04:01.039 Yeah. [Polyèdre]? was saying maybe we can sort information inside each module by importance so that we give a hint on what to know for sure, what is optional. Maybe we can think about categories like that. But we will have to define, "What is the importance?"

S6: 01:04:30.294 And also, when we talk about optionality, that is, okay, not forcing people to actually see the content [and finish it?], but there is also the completion of the course and the certificate. So just also something to take into account into the creating, for example, that you shouldn't be needing those categories, or there should be an easy way to pass them if you want to skip them out so that it doesn't block the completion of the course itself.

S1: 01:04:59.210 Something like minimum requirements to get the certificate, very minimal. And then, well, at least you have two categories: the minimal to get the certificate, and then more things. I think it's also what distinguish between-- so we have different columns here. One is Activities and one is Evaluation. So for now, what we say is that to get the certificate, have evaluation is the minimal requirements that you have to have. You have to pass to get the certificate. And the activities are nongraded, ungraded, but these are very important, anyway, so. But the thing is, on edX, you have the problematic of the paywall. If you don't pay and come as a free audit learner, then you don't have access to-- do you have access to graded elements? I don't remember.

S6: 01:06:25.466 I think you can have graded elements, but you can't have the certificate, if I remember correctly? They have changed that to a few times. So I actually not completely sure what there is right now, especially on edX.org. But within Open edX as a whole, you can definitely even have the normal auditing with the free certificate. But I think on edX.org, they limit the [obtention?] of the certificate.

S1: 01:06:51.847 All right. So we'll have to make sure that most of the content is open to students that don't pay their certificates, at least. That's a very difficult question because we didn't talk about finance and the economic model. But at the beginning, we wanted to say that-- of course, I think we forgot to say that the project is sponsored by Patrick and Lina Drahi Foundation. So a lot of money is coming from IMT, so Institut Mines-Télécom, and they get the money from this foundation. So it's a donator that donates part of the money to make this project happen. So we'll have to discuss with the people giving money what they are open to.

S6: 01:08:02.496 And actually, one of the options we had mentioned for the business model is the mentoring aspect. We haven't really touched on that at all, actually. But I know from Upstream University that it was a really key aspect that, between students who would just be in front of the course content but would not really get any help-- or personalized help, and the ones who did, that there would be a huge difference, to the point that the program is structured around that, around the mentoring. Because a lot of the points that we've discussed before, like judging whether the project is working well, socializing with people, figuring out the right approach, experience is playing a big role in that. And often having someone who has that experience that you can ask questions to, that can tell you when you're going the wrong way or just waiting on that end, can make a big difference. So for the business model, that could be an option, having two tracks, one where you do things on your own and one where you actually get personalized help. Is that something that they would still consider? Is that, yeah, something we could do on edX.org?

S1: 01:09:22.543 I think, yeah. I think so. What do you think, Marc?

S2: 01:09:28.743 Oh, yeah. I think they are open to basically anything. I don't think we would be limited by our sponsors, except if we want to make it all-- I don't know. I think maybe edX.org will have stricter requirements than our sponsors.

S1: 01:09:51.961 Yes. One important thing is the Interaction column. And I was thinking, even, we could think about a kind of professional mentoring with professional mentors that you pay or a peer-grading kind of mentoring, like you find someone that has maybe the same project or the same idea - I don't know - or the same level, or maybe you find an expert. So maybe in that interaction somewhere here, we could think about a "place de marché" or-- I don't know how to say that in English, but--

S2: 01:10:48.418 Marketplace.

S1: 01:10:50.108 Marketplace. Yeah, kind of. "How to connect with other students that could help you?" That's a difficult question.

S6: 01:11:02.842 That's a really great idea because peer-grading works quite well in MOOCs, so it would be interesting to see how that applies to that. And actually, we could even use the modules of peer-grading. It's not necessarily exactly what you were saying, so that might be complementary. But we have those modules. So whenever you have an idea for a project, etc., you could get the review of, maybe, someone else with whom you want to do that, but also maybe a group of other students. But that could be-- for people who can't have a dedicated mentor, that might be a lot better than just sitting down.

S1: 01:11:41.516 So the only mention I can see here is, "Help one or another on the project." So that's the only part where, indeed, you have the mentoring system, "Give help to someone or search for help." So do you think, Xavier, we could use this kind of peer-grading system and tools to search for help or to give help?

S6: 01:12:15.309 Search for help, I think, probably either [inaudible] or a more dedicated marketplace-like tool might be better because the peer-grading is more random in terms of assigning who is reviewing who. But the peer-grading might still be something that comes in addition to that-- is that you discuss with other people that are interested in the same project, but also having a range of opinions, "Have you actually checked all the things? Is that really an active project? Are there [MergePRo?] requests with [inaudible], etc.?" Those are things that could easily become peer-grading criteria because they're quite subjective. And yeah, I think both could be interesting here.

S2: 01:13:03.350 Yeah. The idea in this column was mostly, make a forum so that anyone that wants could help anyone that needs, basically. I did not think about "peer-rating [tips?]." I'm not sure how to say it.

S1: 01:13:29.016 Well, here the tool is really what will enable interaction. So if the tool is a forum, then you have a specific kind of interaction, right? If the tool is peer assessment, then it's another kind of interaction. It's really peer-grading. It's not really meant to be a marketplace. We will have to tweak or to use another tool to have a marketplace or-- yeah. And [Polyèdre?] is saying a chat. So yeah, forum is a little bit more asynchronous, and chats are more synchronous. And of course, we were thinking about live sessions and debates, etc. So that would involve, indeed, chats. But--

S2: 01:14:21.211 Yeah. But could we make a Matrix chat for all the students of the course so that they can freely discuss? Or is it too dangerous moderationwise or--?

S1: 01:14:37.517 I think it really depends how many students you have, but it's feasible. For any project, any communication channel you have to moderate you if something wrong happens. So if it is a forum or a chat, it's the same.

S6: 01:15:05.769 Yeah. And in a way, what we are creating here is a free software project, so we can use kind of the same mechanism. Like, if we have an IRC channel or a chat channel, we can have people within the community that help. I mean, naturally MOOCs structure a little be like that, with some people who are involved than others who can help and be like more community TAs and stuff. So we can't be too afraid of that.

S1: 01:15:32.141 Even for my old MOOCs, I selected the best students so that the following sessions, some of them wanted to moderate and to help the others. So yeah, it's always with a community that you can build on. And some of them are-- some of the students are really so intensively into the course. And I mean, sometimes it's crazy. They work so much to create content or to give feedback, it's just amazing. And I'm pretty sure we'll have the same here-- I hope.

S6: 01:16:17.767 I hope so too. But yeah, it's not sure but likely that, among the first students, they will be already established [first off?] to a contributor that just want to see the content and test the MOOC and probably will get into the forums and be way, way, way above just normal students. I think that will happen in the first session, but I cannot be sure.

S1: 01:16:56.166 So we have to maybe think about this specific public. So if they come to this MOOC-- it's not really the target of the MOOC, but they have to become a sort of contributor for the MOOC. So it has to be clear at the beginning of the MOOC that, if indeed they are experts, not really students, but more contributors, then we have to explain everything and explain that it is open to contribution project; and even if they want to create a specific software for the marketplace, for mentors, etc., well, they are open to do that.

S6: 01:17:42.743 Yeah. And we were talking about partner project that are open to contributions before. Maybe that can also apply to this project. And of course, it would be good to encourage students to go out and try things. But one of the steps could actually be to, I don't know, fix the spelling mistakes somewhere in the project or make a small improvement to the course itself. It would make sense.

S2: 01:18:11.670 It would make sense, yeah.

S1: 01:18:17.473 So maybe in the introduction, what do you think? Can you zoom--?

S2: 01:18:21.971 In the week-- ah, sorry. It was planned in week zero how to contribute.

S1: 01:18:29.296 Okay. And yeah, I don't know if we have to categorize the type of contributions that are possible for the MOOC. Of course, if you go to the forums and help other students, it's a kind of contribution. But also if you go to the source code and modify some of the contents, it's also a contribution. And if you want to develop a specific module to enhance the experience of the MOOC, it's also a contribution. So the different kind of contributions at the beginning-- maybe we could do a categorization of contributions. If you really want to have a big contribution as a partner and you have the manpower to develop something, well, we'll be happy to be open to that.

S6: 01:19:33.000 And I don't know if we'll have time for this time. But one point that could be good also to discuss is governance, both in terms of projects to contribute to in general, but maybe also this specific project, "What does it mean to contribute to it? What's the decision process? Who are the maintainers?" These kind of things could be good to have clearly.

S2: 01:20:02.588 Yeah. I put a small paragraph about governance in the report, but it's very basic. Let's see where it is.
[silence]

S2: 01:20:33.501 Yeah. I basically titled this "Decision Process" and said that, basically, if there is a doubt, try to discuss it and reach some consensus à la Wikipedia. And if there is a fundamental difference in what people expect from a section to convey, then-- as long as the project doesn't have a fixed board or governance or organization with long-term contributors, basically, I put that I will decide for now, but I expect that to change in a few months when we will see who is a regular contributor and who brings what to the project. And at that point, I think we can revise that and have a more egalitarian decision process. But currently, in my opinion, the BDFL is a good way to start putting direction in a project.

S6: 01:21:49.498 Yeah. That sounds [inaudible]. But I mean, it's pretty much the impression that I got, but it's good that it's written clearly. So yeah, I think that sounds good.

S1: 01:22:02.322 Okay.

S2: 01:22:03.238 So I hope I will be a good dictator, but. And that could be the final words of the session since it's 8:00 PM.

S6: 01:22:14.863 Good conclusion.

S2: 01:22:17.252 Okay. Does anyone has any remarks? Charlène or Jérémy, you did not talk a lot. So if you want to express what you think or want to say about the project, feel free.

S5: 01:22:32.651 No. I was just thinking that maybe-- you were saying that the Git-- the second module could be optional. But I think the first one could be optional too, maybe, in the sense that the real goal of the MOOC is to know how to contribute to a FLOSS project. So we can imagine that some people would come and would already know about FLOSS and about Git. And maybe a way to-- because [Polyèdre?] was saying that we can order the important information and the one that are optional. Maybe a quiz at the end with the important information would be a way to-- I mean, if someone already knows about Git and wants a certification, he can just go straight to the quiz and try it. And if he succeed, then he can go to the heart of the MOOC.

S2: 01:23:31.974 I think that's already possible in edX, to just do Next, Next, Next, and do the evaluations.

S1: 01:23:39.712 Right. But we will have to explain that clearly, explicitly, that, "Just go to these parts, do the quiz, and then move on," something like that. Yes. Very good point. Thank you.

S5: 01:23:58.413 Because when I read the personas, and I was not the public on the MOOC. But I think that the Git module could be interesting for people who just want to learn more about it. And also the first one about FLOSS, personally, I'm unable to do the three, four, five, etc. modules, but I will definitely be interested in doing the first one, for example. So I was imagining that, because everybody can contribute to the project, somehow the first module and the second module can be taken out and become a MOOC in itself.

S1: 01:24:42.006 In itself. Okay.

S5: 01:24:43.066 So I think it's really important to know what is important for people who want to succeed in this MOOC, which is about contributing, really, for IT people. And why not adding some more information for other people like me, for example? But I don't know. I'm getting a bit unclear.

S1: 01:25:06.862 No, no. It's clear. Yes.

S5: 01:25:08.506 Yeah. Because if I have understood, in the next brainstorming session, that will be about the content of the first or the second week? I don't remember. No, the first one?

S1: 01:25:21.370 Yeah, the first week?

S2: 01:25:23.215 So the week one.

S5: 01:25:24.788 Yeah, the week one. So we will be talking about the contents. It would be very important to distinguish what is really important for someone who wants to succeed the MOOC to know, and to let aside what is interesting but not important for the goal, then.

S1: 01:25:47.266 Okay. Thank you. Very good remark. Thank you. We'll keep that in mind. And the next session is tomorrow, I think, right?

S2: 01:26:03.207 I think so.

S1: 01:26:03.978 Yeah. Any other--

S6: 01:26:06.764 Actually, the dates--

S1: 01:26:06.764 --remarks? Yes?

S6: 01:26:09.252 --are on the Issues on the repository, is that correct?

S1: 01:26:13.581 Yes.

S6: 01:26:13.581 Because I only saw the date for this one, actually, for now. But okay, I'll have a look now.

S1: 01:26:18.392 Yeah. All of them are in Issues in the repository. Yeah. That's what you show here, Marc. So you have the dates and the content for each [crosstalk]--

S2: 01:26:34.024 It's one issue per session.

S1: 01:26:38.964 Per session, yeah. All right. So any other remarks, or we say goodbye?

S2: 01:26:51.357 Yep. Goodbye.

S6: 01:26:54.272 And thanks for all of that. It's really great. Can't wait to see the course [one of these days?]. It's going to be nice.

S1: 01:27:00.083 Yeah. It's a good start. Thank you all for joining this starting session. It's a kickoff session. So I'm still very happy about this project. It is very nice. So thank you all, and I hope to see you in other sessions with Marc and maybe Xavier.

S6: 01:27:21.580 Sounds good. Yeah. See you tomorrow.

S2: 01:27:24.170 Okay. So thank you, everyone. And see you next time, I hope.

S1: 01:27:30.080 Yes. Bye-bye.

S3: 01:27:31.013 Bye-bye.

S1: 01:27:31.460 Thank you.
[silence]

S2: 01:27:40.818 Okay. I stop the recording.
