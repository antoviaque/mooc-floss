# Brainstorm - Module 3 (Meeting Transcript)

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1613669413163

S1: 00:00:00.000 [foreign]. Hello and welcome everyone to this brainstorming session. The fourth one for the MOOC FLOSS. [inaudible] an open-source software. So today is a brainstorm on what we call week three but also module. And as you can see on the screen, this module is about who is involved in the FLOSS community, organizations, what licenses, what economic models they have, charities, companies. We'll have to get ideas. So the idea here is to gather ideas on contents that it can be read, listened to-- that we can listen to like a video, a text, of course, we can read, any kind of resources, any kind of name of organization, of projects, of anything we have as ideas. Also, to think about what type of interaction we would like to have in this third module. For example, debate on the comparison of licenses, any other kind of interaction between students or students and mentors or students and professors or students and the outside world, meaning outside of the MOOC. Also, thinking about activities both in terms of activities that are-- just to do something instead of only reading or listening or debating on the floors. Activities that you can really do with a tool. And of course, the second type of activity is evaluated so that the grades you have at the end of the activity will count for the certificate at the end of the course. So this is what we'll be discussing about the brainstorming of today. Yes, we have a Framapad. So it's a collaborative document. You can click on the link that you just see in the chat and you are welcome to note any idea you have. So I suggest we complete the Framapad-- I mean, the document, starting at the end of the document so that we have at the beginning the older brainstorms. What we do is at the end of each brainstorm, we take what we have put in the document and sort of create a condensed version of everything that was said. And Xavier was also suggesting that we will have transcripts of all the recorded sessions so that we don't miss any information that was given during the session. And I think it's a good idea. So the agenda for today-- do we have an agenda?

S2: 00:03:26.474 Yes, we have a rough agenda in the issue. I can paste it.

S1: 00:03:32.924 Yes, please. So anyway, we start with a very brief introduction of all the participants. Let's say 5, 10, or maximum 15 seconds to present yourself. And so I can start myself. I'm Rémi Sharrock. I work at Silicon Valley, an engineering school near Paris. And I am in the computer science department. I created many MOOCs and I'm really interested in this new MOOC project because it is contributive from the very beginning. As you can see, we do brainstorms to get ideas on what to put in this future MOOC. Anna?

S3: 00:04:20.851 Thank you, Rémi. So my name is Anna. I work in the IMT MOOC teams. So I'm supervising the MOOC projects in different IMT schools. So basically my responsibilities are transmitting the methodology of MOOCs, the pedagogical part, interacting with the platforms, and also everything that has to do with the financing and the strategy of MOOC projects.

S1: 00:04:51.954 Thank you. Loïc?

S4: 00:04:57.325 So I'm Loïc Dachary. And around 2013, I created a course about upstream contribution to teach developers how to contribute to free software projects. It ran for a few years, two or three years, I think. And there still are some leftovers or transformations of this course nowadays most notably in the OpenStack community. That's it.

S1: 00:05:45.491 Oops, sorry. I mute it. Olivier?

S5: 00:05:50.300 Hi, around 20 years ago, we created stuff with Loïc. No, no, no, we are old timers and also an old timer in free software, but I also worked in IMT together with you guys and Anna as a research engineer, teacher, and also, passionate about free software, open source, and so on. So I'm also participating to a MOOC on Git. That is by complimentary. So, yeah, I'm here to try and help.

S1: 00:06:29.741 All right. Thank you. Xavier?

S6: 00:06:33.994 I am Xavier Antoviaque. I'm from OpenCraft and the Open edX community. So I'm also an old timer, I guess, in open source and free software, 20, 25 years, also. Not as old as Loïc. That's not possible, but Olivier, I don't know you enough yet to make this kind of comment. But, yeah, basically, within Open edX, we have this project also to introductory courses for new contributors and upstreaming. So that was the idea to try to pull resources together and maybe work together on something that we would be able to share between projects. So that's why we started discussing with Marc and Rémi and with Loïc and also with OpenStack more recently. Yeah, and on our side, we would like to, yeah, develop that a little bit further. Also, OpenCraft, being a company, we want to make sure that we do that in a sustainable way so that we can keep putting resources and improving the MOOC over time. So, yeah, that's why.

S1: 00:08:02.309 And then we have polyedre, still here.

S7: 00:08:06.045 Yes, so, hi, I'm polyedre. I'm a computer science student. And I'm here to beta test the MOOC and try to understand what you want to predict as a student. Thank you.

S1: 00:08:20.364 Thank you for coming. And, Marc?

S2: 00:08:24.218 Hi, I'm Marc Jeanmougin. I'm a research engineer at Télécom Paris. And I wanted to create this MOOC so I partnered with Rémi who is a MOOC expert. And I'm also a contributor to Inkscape's free software project.

S1: 00:08:43.795 All right. Thank you for the introduction everyone. Next, is what are the main ideas we want to convey in this module. So as you can see here, so who? Organizations, licenses, economic models, charities, personal debates on licenses. As an activity, we would like the students to find examples of organizations, economic models, etc., find projects. So during the last session, we were discussing a lot about finding the projects as being the very first activity of this MOOC. Indeed, some of the ideas we got before was that week zero, one, and two almost was about polishing prerequisites for this MOOC. And that's the real activity for the MOOC would start, indeed, during week three. And the real activity is indeed to start by finding a project so that at the end of the course, you contribute to the project. All right. And some evaluation, we would like to have ideas on how to evaluate this kind of activity maybe a multiple-choice question, maybe something more funky. So any idea is just welcome. First, you know what, we could list any organization that you know that are waiting with free software or-- I'm pretty sure we can put a lot of them in the pad as a start.

S2: 00:10:34.363 You mean communities?

S1: 00:10:36.218 Communities and also listing of-- yeah, communities, maybe they have a list of companies and communities.

S2: 00:10:54.163 Because I think companies is some sort of community, I guess.

S5: 00:11:06.933 That's quite interesting. [laughter] It could've been a joke or a starter for some discussion a long time ago because at some point in time, there were just editors where company or project wasn't any different, but nowadays, there are many companies acting in projects and not so many projects just with one single company. So I don't know if it is up to date to use that as a software, but that could be a question to deconstruct opinions and so on.

S6: 00:11:49.096 Yeah, I agree that, but that might be an interesting part to start out because at the end, we contribute to a project, right? So it's important to be aware of the actors on it. So individuals, organizations, companies, and etc. But we want to redirect the students toward the list of organization or towards a list of project because in the past, actually, we mentioned a list of free software projects, right?

S1: 00:12:26.355 That's right, and we were also thinking about a kind of the repertoire in which you could search with a search engine. So, yeah, and that could be also integrated with the GitLab so that it is static and everything, everything. So we have this kind of--

S2: 00:12:50.138 Search for projects, right?

S1: 00:12:54.869 Search what?

S2: 00:12:56.430 For projects in that organizations?

S1: 00:12:59.121 Yes, yes. Well, and if we have a project that is linked to an organization, it could be a colon in the repertoire-- in the directory.

S6: 00:13:18.486 Directory.

S1: 00:13:20.937 I like the word repertoire because it sounds French.

S2: 00:13:27.751 It is French.
[silence]

S2: 00:13:39.844 So your idea is that we list organizations or projects? Because--

S1: 00:13:46.049 Both of them.

S2: 00:13:46.499 --basically, organizations and communities are the people around projects and an organization can be around lots of projects. For instance, KDE can be seen as an organization even if it has several organizations on specific projects like Télécom Paris, [Krita?], etc. They have some sort of suborganizations in organizations.

S5: 00:14:17.597 If I may, just to clarify a bit the vocabulary. I would put a few different concepts. One could be a project which could be either a grand project or a small project, meaning a [inaudible] or an executable depending on many projects. Then the communities, which can be the people gathering around the project, either as users or contributors or value stakeholders. So it's a large community of interest and so on. Not only are the ones developing. And then there is a child in the back. I may shut the webcam, yes.

S1: 00:15:00.702 [crosstalk].

S5: 00:15:00.438 And then there are also organizations. Organizations, meaning bylaws, associations, people that own the website, and so on. So the people that can decide on all the [legalities?]. The people that may hire [crosstalk] so on.

S2: 00:15:17.829 And companies.

S5: 00:15:19.488 Yeah, and then companies which are a subgroup either/or are individuals and organizations and companies, okay? There's a bit of background noise. I'm sorry. So I think there are three different concepts here because the roles are a bit different because you can have a community of people that are not registered. They are just passersby but they are interested in the project. They are people who are responsible because it's their duty to manage [legalities?] and so on. And they are just bytes and documents and artifacts that are in the repository. And the people can be seen as artifacts or as assets of the project, of course, and so on. So maybe if we have to present a little bit all the relationships in the different roles, maybe there can be a listing of objects, a listing of organizations, a listing of companies or profiles of the [roster?] and so on. And we could do kind of a matching game to see who's both in a company and contributing to several projects, or is there a single company managing several projects and so on.

S1: 00:16:48.268 Interesting idea.
[silence]

S1: 00:17:02.021 And we could even ask as an activity by giving one project to find the name of organizations, companies, or-- Loïc, you're right, fiduciary umbrellas, which is also a great idea and by giving an organization what project do they have, etc., so. Interesting.

S4: 00:17:38.130 It could be interesting to-- let's say, every participant in the course has a project in mind. It would be interesting to ask them to look at the project and everyone involved and try to associate the people or organizations involved to the categories that were listed, like is it a company or is it an informal group, are they a group of friends, is there a fiduciary umbrella. It's often invisible. So it's interesting to look at that. And in the end, it will help them know the community better and sort out which is what.

S6: 00:18:32.001 And actually, I think the two type of exercise that you've described are actually quite complementary because if we come in and say, "Okay, look at these specific project and tell us what it is," then we know the answer. So we can, basically, without the presence of a mentor or anything, test on this and erect explanation and make that part of the MOOC itself. Then if once they have done that, if they do that for the project that they have chosen, they already have in mind kind of a good example which we can choose a bit complex or different examples so that they have structures in mind. And then as a group, like you mentioned, populate even maybe a database, actually, because then we can have a wiki where different runs of the course, people will populate it and etc., which is kind of an interesting contribution by itself if it doesn't already exist. And also--

S4: 00:19:31.094 It actually exists. It's in Wikidata.

S6: 00:19:36.046 Well, but with that type of categorization that-- I mean, we could have it like it populates or improve.

S4: 00:19:43.092 You can do it backward, but there are things in Wikidata already. And you could say, "Okay, whatever is in Wikidata that categorizes projects and organizations, that's what we take." And if there is not, then we add something, but you don't have to create it. Or you can even ask the students, "If you discover something that is not in Wikidata, then you add it." It's part of the assignment. And so it builds itself over time.

S5: 00:20:22.323 Out of curiosity, Loïc, do you know that there is an ontology dedicated on describing saturations and so on?

S4: 00:20:30.524 So that's the point of the URL inserted before. It's the ontology. So you define the ontology in a Wikidata way that is very informal--

S5: 00:20:41.877 And you pasted that in the pad?

S4: 00:20:48.649 Oh, no, it's not in the pad. It's in the chat.

S5: 00:20:50.941 Yeah, but, I mean--

S4: 00:20:52.737 Yeah, it's actually very interesting to do that and not very complex.

S6: 00:21:04.314 And beyond the structure of the data, have you looked at some example? Does that make sense what is asked in our IT done? Do you have an example of what we put in front of the students? Are they going to make any sense to them? Is that going to help them with our goal here?

S4: 00:21:25.427 I think so, yes. From the top of my head-- because I worked on it maybe five months. That's a few years ago. So the goal was to really make it possible for a human or a bot to figure out quantitatively where free-software project is. And in particular, with its relationship to organizations. But for instance, for every free-software project, there is the number of people who are contributing to the project. So that's quantitative. How many people. And there is also the affiliation of these people to a company. So that's another number. And then you can say, "Okay." And then you have a link to the company or to the organization and then you can figure things out.

S6: 00:22:29.472 So that's a very new question because I've never used it this way. Where would I look if I want to see the current entries for Open edX, for example?

S4: 00:22:39.425 You have to query [crosstalk]--

S2: 00:22:44.105 Look on the infobox of Open edX in Wikipedia which are usually pulled from Wikidata.

S5: 00:22:51.237 Loïc may share his screen maybe.

S4: 00:22:53.706 Sorry?

S5: 00:22:55.189 Loïc, you may share your screen maybe.

S4: 00:22:58.206 It's from the top of my head. It's not on my screen. There are queries--

S1: 00:23:07.647 I can see a try link-- try its link under some of the queries like list of our FLOSS. If you click on Try It, then it goes to the Wikidata query service and you can execute the query and get the answers.

S5: 00:23:35.032 But, I guess, the idea wouldn't be to ask the participants in the MOOC to try this tool because they need prior knowledge of what Wikidata is and so on. But if they use Wikipedia or other sources of information, they can find information, and then we can confirm that to what is in Wikidata. And maybe there are mistakes or inconsistencies. It could be a way to provide feedback into Wikipedia, which could in turn be good for [crosstalk]--

S2: 00:24:01.070 But if the data is reliable and the service is reliable, we can use GitLab's CI to pull the information and rebuild it every time we want to update it and have it as a table or something under MOOC.

S1: 00:24:25.613 So I can see the Open edX platform exactly.

S6: 00:24:30.929 All right, so what do you see exactly?

S1: 00:24:34.296 Can I share my screen?

S2: 00:24:36.046 Yes. Sure.

S1: 00:24:36.766 [There's a lock?]

S2: 00:24:37.858 Yeah, yeah, one second.

S1: 00:24:41.664 Let me click this one. All right, yeah, so what I did is I clicked on the link that had an example of a query on FLOSS projects. Then I got 12,000 results. And then I search here in the box edX. And I got the Open edX platform here. And also, this took to the XML editor.

S6: 00:25:22.337 Okay, yeah, so that means that for each of the attributes that would be a specific query. And then to edit it-- yeah, okay. I see. That's--

S1: 00:25:34.113 And then you have a-- yeah, that's interesting. Statements, instant of learning methods, system, free software. And it may-- what? Oh, that's an instance, right. [crosstalk] ontology because-- interesting. [crosstalk].

S4: 00:25:51.295 Oh, yes, and so an idea would be-- it's very likely that the information-- for the assignment of the participant, it's very likely that the information that they gather is not already in Wikidata. And so the outcome of the assignment would be put it in Wikidata. And then the validation would be to just make a query in Wikidata and see that there is nothing or that it's consistent. And maybe it could be peer reviewed. It does not matter much if it's correct, I think.

S1: 00:26:34.384 So how--?

S4: 00:26:34.720 But it's a matter of judgment and it can change over time.

S1: 00:26:38.435 So how does it work, Loïc, when you modify something on Wikidata? You edit and then--

S4: 00:26:48.754 I'm sorry.

S1: 00:26:48.764 So you have to have an account or--?

S4: 00:26:50.637 Yeah, you create an account and then you edit and it's done.

S1: 00:26:54.632 Okay, pretty easy. Okay.

S5: 00:26:58.945 But that's the--

S4: 00:27:00.299 Now, what is really difficult and confusing is it's a database but we're talking about developers, which shouldn't be too much of a problem for them.

S2: 00:27:14.752 It can even edit anonymously. It's like Wikipedia.You just click on--

S5: 00:27:22.671 I'm not sure-- I'm not sure it is the best way to do the exercise, but it could be interesting because we have to limit the number of tools that people will be confronted to and so on and having to explain them how to use a tool and avoiding messy use of the tool for the project. So, okay, we would have to calibrate that.

S4: 00:27:45.907 That's true. Yeah, [crosstalk]--

S2: 00:27:48.077 I think--

S5: 00:27:48.334 But at least--

S2: 00:27:48.478 --Wikidata is a convenient way to pulling information to show to the students but maybe not a tool for the students.

S5: 00:27:55.583 Yeah, that's what I mean.

S6: 00:27:57.919 Well, I don't support that because--

S5: 00:27:58.851 And also, we can just ask people to read the pages in Wikipedia, which is less formatted, less structured, and so on. And then probably, they can have the same information. And by the way, they read and they're on something during that. So not just--

S6: 00:28:20.166 Well the--

S5: 00:28:20.923 --solving a puzzle. I mean, making queries is like solving a puzzle. And I would prefer that they learn something by reading and interpreting but depends on the category of people. If they know already about ontologies, queries, and stuff.

S6: 00:28:33.801 Since we are in a course about upstreaming, I think something as simple as a contribution to Wikipedia should be definitely something within scope. If we can't have a mechanism where the students wouldn't be doing anything or we can't trust the students to do this kind of thing as well, it probably means we are not doing our job very well, I would say. So it's true that there are risks for people doing nothing, but, I mean, that's also what people were saying about Wikipedia at the beginning like, "Anyone can come and put bullshit in there." So one thing that I like about that approach is the fact that it's the first way of being confronted to a contribution, like one where there is no code, where it's very light. It's one of the most basic things, and it's the first thing they can do pretty easily and early on during the process. And maybe they will get it reverted, maybe there will be problems and etc., but that's actually part of what we want to learn here, so, yeah.

S2: 00:29:38.458 Yeah, that's actually a very good point because in the previous week we have a test contribution to some GitLab so that we would host, and it's not really a public thing since it's even within the MOOC, but here, asking-- even to contribute like adding some information to Wikipedia because Wikipedia doesn't have a lot of information on free software projects usually. Many, many pages of Wikipedia are not really up to date for free software projects. It could be an interesting ask to have a public contribution somewhere, like a first step into making something public even if it's not good.

S4: 00:30:22.233 And also, one of the questions I asked at the beginning of the course was if people contributed to Wikipedia, and most of them didn't contribute even once to Wikipedia, so.

S1: 00:30:41.787 Even just to see how it is to-- how one article in Wikipedia has changed over time, who were the initial contributors to the discussions, what happens behind the scenes. Not only you consume the data from Wikipedia but you know how it works behind the scenes and how it is indeed a contribution platform.

S4: 00:31:14.319 It's also a very interesting example because it's one of the most hostile community I know. And still, it's the most well known, and it works despite of that. So that puts a lot of things in perspective.

S1: 00:31:36.748 Yeah, so indeed they have contributing to the Wikipedia page with the--

S3: 00:31:45.202 Actually, it also could be something that the learners could share between each other. They could share maybe screenshots of what they did on Wikipedia and have discussion forums on that. So it wouldn't be completely-- like an activity that is lost on Wikipedia because I think there will be a lot of questions about how to do that in the course.

S6: 00:32:08.721 Yeah, yeah, that's a good point. And actually, even we could also have our own mechanism for peer reviews because we have that within MOOC. So if people have to post the link to the contributions they have made, there could be other students looking at it. So we could detect maybe some of the bad cases more easily. And probably the project themselves would appreciate that we have those steps. If we have some people abusing, we can say, "Okay, we asked doing that too." And also, I guess, the second benefit of this is that it also helps putting people into the shoes of a reviewer, the person who receive the contribution if they have to see the contribution of other people and to review some other students.

S5: 00:32:59.461 Don't you think that the barrier to entry or the prerequisites to do such activity wouldn't be too high? Meaning that somehow we are replicating a MOOC that is about contributing to Wikipedia. And there are already MOOCs on that.

S1: 00:33:16.791 Really? Which one?

S5: 00:33:19.603 There's one open MOOC, I guess.

S6: 00:33:20.370 From the Wikimedia Foundation of France. I mean, we don't need to have the whole thing about contributing to Wikipedia. It's a very specific use case especially if we focus on the Wikidata. But you're right about one thing is that there will be lots of areas where we'll have intersections with things that communities do already. I mean, we already have that with Open edX a little bit because we'll want maybe to have that entry point. And so we don't need necessary to just be completely self-contained. We could also refer to some other courses if they want to learn more about that and direct toward them. Maybe even work with Wikimedia Foundation to see, "Okay, so you have that MOOC about contributing, what are the common points? What can we do together?" Actually, I'll write it down as a good idea may be to write to them, actually, because I had talked with them at the time when they add on that MOOC and I did not remember that.

S3: 00:34:24.790 Once again, is it a MOOC on FUN-MOOC? Is that what you said, Olivier?

S1: 00:34:28.581 Yeah, I put the link in the framapad.

S3: 00:34:31.280 Was that MOOC in an English-speaking project?

S6: 00:34:36.129 Yeah, they were actually planning to have an English for that on their own instance of Open edX. That's why we were discussing, actually. We're with them at the time. The problem is that the person was in charge of that at Wikimedia France left in the middle of the project and nobody took it off. So it never really went beyond that. But they have the idea or at least some desire to make it more international or english, etc.

S5: 00:35:09.434 In any case, it can be interesting to test that to prototype and see if pedagogically, the aspects, it is a good-- for sure, a contribution to clarify pages and make links and so on about the project is a fine contribution. So in any case, that's like labor. So it is just that if we have a troll or disputes that people are stepping in without prior knowledge, it could be funny. Like saying that Ubuntu owns debian and noted to the Wikipedia page, and of course, it would be funny to see the end result. But okay, let's try it and proceed.

S4: 00:36:00.005 I think you have a very good point where it's about the little complexity involved. It's easy to dive in Wikipedia and lose yourself in it. So it should be something that the course does not encourage. However, for every free software project that the participants are likely to contribute, there is a Wikipedia page. So it's part of the project, really. The Wikipedia page is somewhere they will contribute eventually, of course. And the Wikidata that supports that Wikipedia page is also in the scope. And so it's a matter of going a little bit beyond the scope but not too much.

S5: 00:36:53.185 I have one concern about such activities, though. I think it wasn't mentioned already is that if we think about multiple editions of the course, we shouldn't have resources that are not usable anymore after the first edition. I mean, if they are low-hanging fruit for the first edition that people get rid of, then there would be nothing for the next edition, which can be problematic. But that's probably the same for many types of contributions that we can think of like fixing bugs or contributing whatever. Of course, after the first batch, maybe there is no more. The project doesn't evolve in between, so. But--

S2: 00:37:41.897 I don't think we can finish the project free software of Wikipedia in one year or even in two years or so.

S4: 00:37:53.230 I think what Olivier was suggesting is that the course shouldn't contain-- pointed to a specific project or a specific task. But coming back to what project participants choose to contribute to. If the participant comes with a given project they want to contribute to and are given a contribution they want to make given by Crick's, then you won't have this problem. and the course wouldn't contain anything that is specific to a given project or a given contribution, which is good long term.

S2: 00:38:34.356 Yeah, and so the thing is we don't really even care that modification to Wikipedia is about free software organizations. I mean, if it's about publishing openly, like something they did, it could be about anything. It could be about improving any Wikipedia page, but it's better if it fits the MOOC because that what they are learning about, but if they are an expert of some topic and they want to improve the Wikipedia pages on this topic, then really, I don't see this as problematic.

S5: 00:39:17.752 Okay, but then we have to plan the first activity early on. That is just contribute to anything in Wikipedia, be it your preferred [literary?] role or whatever, in a sandbox or in the public pages, depending. So people get to learn the tools. And then in week three, after we have maybe already passed some kind of validation of the ability to do so, then we can edit something that is on topic for week three.

S4: 00:39:49.209 It's excellent and the goal would be twofold. First, it's an excellent way to an icebreaker because then the first thing you do in the course is you contribute where it's the easiest and where you know best. Say, "Okay, I did it. Let's try to do more."

S3: 00:40:16.120 Yeah, actually, if you will have pages that are not specifically related to anything and it's just some topics that interest learners, I am pretty sure they will be very happy to share because it's always like, "Oh, yeah, I created a page on this," or, "I contributed on that." And it's - I don't know - a hobby of mine, so.

S2: 00:40:36.721 But that's also the highest chance of having a revert.

S4: 00:40:45.138 True, then [crosstalk]--

S6: 00:40:48.104 Which is a good-- which is a good experience too, right? I mean, you need to feel that pain sometimes.

S5: 00:40:54.330 That's why we have to do it as an exercise early on where we explain that you're contributing to some common good where people are governing and so on where there is a review process and so on. And you prepare them to see it discarded. And once you have explained that, now, we focus on something that is more valuable where they have more expertise may be, which is the analysis of relationship between a project and community and a company and so on. And there, there are lesser chances that their edits are discarded, but maybe they have a higher effort to provide a more qualified data. And they will see that's what--

S2: 00:41:31.223 Yes, the more small and sourced the chance and the highest chances it can stay.

S6: 00:41:41.253 And actually, in Wikidata, others that work for the sourcing of the data, how do you manage that?

S4: 00:41:50.473 So it's fairly lax, but for every data you put, you are supposed to add a link to the source. So you cannot put just a number there, but you do have a link to where you found that.

S6: 00:42:11.456 Okay, makes sense.

S4: 00:42:12.217 It can really be tricky sometimes.

S1: 00:42:15.268 And can you modify or upload a new ontology?

S4: 00:42:22.673 So, no, you do it manually, mostly. The approach of Wikidata for ontologies is very peculiar. It's handmade, very messy, yeah.

S1: 00:42:43.204 It is standard like OWL or--?

S4: 00:42:48.466 No, it's absolutely not standard. It's bottom up. So, yeah, it's a shock to everyone who knows ontologies.

S2: 00:43:02.537 Oh, and if you want to know more about that, you can talk to Fabian Suchanek who is working a lot on anthologies in my team.

S1: 00:43:14.885 So anyway, I think the ontology that you've shown, Loïc, gives ideas that we could show. We never had this idea based on-- like you have a project and in his story, we were talking about [talking?] projects. Maybe that's the concept behind it is based on, but also, it is kind of-- when you have a big FLOSS project software and it could be a dependency link or it is very complex to see all the links with libraries, other projects based on Fox. So a lot of concepts here are involved. Parts of, facet of, instance of movement.

S5: 00:44:27.191 There are plenty of things on the agenda, and I see that the time is running. So we have discussed an activity-- or activities around trying to document stuff and also contributing to Wikipedia other wipedia or whatever the title is or stuff. So basically, the goal is to discover, to understand, and to create crisis that we could evaluate maybe. But on all the other matters like licenses and all that stuff that is all the legal aspects that are quite important, I guess, is it still in the focus of the meeting today?

S1: 00:45:15.209 Yes.

S2: 00:45:15.421 Yes, but, I mean, all we discussed is about activities and interactions. And I think license issues too, well, might be less suited than what we talked about for interactions and activities.

S5: 00:45:35.931 I'm sorry, might be?

S2: 00:45:37.994 I think it's harder to find interactions or activities around licenses or economic models, but we'll definitely need to talk about them and find how to teach things about licenses and economic models in this week, yeah.

S4: 00:45:59.176 Well, you could have very intense debates if you compare licenses.

S2: 00:46:05.909 Yes, but I don't want trolls. I don't want too many trolls because I cannot avoid-- I'm pretty sure we will have a troll on SSPL or something like that, and that cannot be avoided.

S5: 00:46:21.000 Troll is already an activity if you think about-- I don't know if everyone is familiar with the Blumer model of-- what is it?

S1: 00:46:32.990 Taxonomy?

S5: 00:46:34.099 Taxonomy of activities and so on. Trolling is very high. You have to understand stuff very well to be able to invent traps into which people will fall to make fun of them and so on. So I guess, before the trolls, we can think about just educating people a little bit making sure they understand the business. And then we can expect, of course, that some people that are well aware will be trolls. But I'm more concern about explaining free, nonfree, copyrighted, and so on, and due licensing and why you should not pay an unpaid license and so on and so forth. And also, giving--

S1: 00:47:12.549 Choose a licensed--

S5: 00:47:15.887 --[crosstalk]. So--

S2: 00:47:18.906 My first idea about that would be to replicate what we talked about in week one about what is free software, and maybe try to get experts on licenses. I was thinking about Bradley Quinn to talk about these topics or people more known that are with maybe infographics or animations or--

S3: 00:47:47.237 Maybe as an activity, I propose to make a case study, for example, to analyze some of the products that your learners use in terms of license and economic model and kind of understand why one is using one license and the other is not.

S2: 00:48:06.539 It is very hard and very linked to economic models. It's very tricky to ask someone to understand why some project is dual license, basically, before having explained, basically, what implies the fact that some license is AGPL and why it has to be dual licensed to be used by some companies which do not want AGPL code or stuff like that. People can find this information, but I think it's faster if we give this provision in the MOOC rather than ask people to find it. I mean, both are doable.

S3: 00:48:50.473 [crosstalk] prebuilt case studies where they have to analyze some parts of them, for example. I don't know. Like you give some part of information and they kind of deduct the logic behind it. [crosstalk]--

S6: 00:49:03.352 Yes, it's somewhere between--

S3: 00:49:05.307 --the topic but I'm thinking out loud.

S6: 00:49:09.655 Somewhere between the two could be a sweet spot, yeah, because it's true on one side that you can't really understand those logic if you don't have a little bit of background, but at the same time, if you get just like someone talking about the legal point of view, explain in general what are the aspects, it's going to be very dry and a bit boring, to be honest. So talking about it in the context of specific use case and scenarios might be a good way to do that.

S1: 00:49:41.017 Right, so you invent a use case and you ask at the end, "So what is the best license option for this use case?" That could be a-- it could be interesting.

S6: 00:49:53.485 That one might be tricky because people-- that's when you'll get the trolls, though, because depending on your philosophy that the competition is going to be very different. I would rather start from existing cases and explain why those actors took those decision without-- or remaining neutral about whether that was right or not.

S2: 00:50:16.214 Yeah, and we can take a very different ones. Examples like Google Project with heavy CLAs or Inkscape project with just GPL and not dual license or nothing required to contribute.

S1: 00:50:38.191 And I just--

S6: 00:50:38.473 And just about the trolls earlier, I like the idea of talking about who the troll is before we get into the topics. I want that kind of generic trolls to explain that is actually a role that as a contributor, you probably want to avoid. And making that a bad figure before people are tempted to do that. I kind of like that concept, actually.

S5: 00:51:03.762 You mean we have to have a sociological or anthropological contributions about trolling in free software, is it?

S6: 00:51:15.886 I wouldn't be against it. I like that.

S2: 00:51:19.412 Trolling is essential activity.

S5: 00:51:22.225 I don't know if you're familiar with the works of Biella Coleman, Gabriella Coleman with the focus [inaudible] at [UCan?] in Quebec in Montreal. No, it's not [UCan?]. It's another one. Anyway, in Montreal. And she has affinity to a website that is called hack.qu-- something hackqu. And there is plenty of contents about hacking, trolling, and so on. So I guess, maybe that was mentioned in the earlier editions of this installment. I didn't participate in but I guess, for culture and understanding all that - Yeah, thank, Marc - there is plenty of, yeah, trolling, hacking, and so on in the hacker culture that was popularized a long time ago by ESR and so on. So maybe that's where we can sometimes say, "Okay, this is a troll. Be prepared and we explained what a troll is and then, okay, go. Go and troll."

S2: 00:52:32.236 It's a dangerous task because you need to be really knowledgeable about a subject before trolling. Except if you choose an easy one like Vim versus Emacs or--

S5: 00:52:45.306 Yes, but what is for sure is that a new contributor in a project can be confronted to that. So we should prepare them not the hard way that they may step into dangerous waters and--

S2: 00:52:57.815 Like recognize a troll and know how to act on it could be a MOOC in itself. [laughter]

S4: 00:53:05.594 There is one activity which would be for the project chosen by the participants. The license and the process to accept contribution, how does it affect their contribution. So it's a fairly easy one. And sometimes it's perfectly okay, but it has to be written down, say, "Okay, there is no CLA. I keep my copyright and the license is compatible with the copyright I want to put. So I'm good. I can contribute." There is another activity that I did which is very weird but worked really well in universities. So with people actually face to face so maybe it could be done with two people over videoconference. It's to take a license. Read one line to the other. Explaining it to them. And then the roles are reversed. And I was surprised. The first time I tried, it was on the GPL. It lasted two hours. And the students were actually listening and participating because they had to explain. And the sentences are fairly easy to understand in themselves. And after a while, it starts to tell you a story. So that did work, but there were 15 and 20 people in the same rooms. So I have no clue if it would work in a MOOC.

S2: 00:54:59.430 Maybe in a forum activity?

S5: 00:55:02.473 So this has to be synchronous?

S6: 00:55:08.051 Not necessarily. You could even imagine a type of exercise which splits the licenses into multiple parts and different parts are spread to different students. So something like that.
[silence]

S1: 00:55:31.119 polyedre also was raising a point like the difference between licenses related to code versus about related to content. And we have--

S6: 00:55:44.029 This is actually a great segue into a point I have - let me know if we don't have time for this today - which is the license of this MOOC because one thing-- that might be a very good troll actually. So if we start only on Creative Commons license, one thing that will be missing in any of the licenses - yeah, that's great; we have anonymous with us - is the disposition from the HPL which include that if you are showing the cause somewhere, you need to include the source of it. So if someone takes our cause, publishes it somewhere else but doesn't publish their changes, we-- it might be quite tricky in some cases especially for exercise our interactive parts to actually get back that change even if the license remain, the Creative Commons. So, yeah, I just wanted to put that out there because that's something that is being discussed in the Open edX side between AGPL and Creative Commons. So, yeah, that's a topic actually, I think even for us what you had set forth yet.

S2: 00:57:03.042 Yeah, so it's CC by-SA. So even if they rehearse it, they have to rehearse it on CC by-SA.

S6: 00:57:15.300 Yeah, but the fact that they hosted somewhere doesn't imply distribution, right, at least for the interactive parts.

S2: 00:57:27.016 They can even redistribute it privately.

S4: 00:57:30.932 No, but I think what Xavier is trying to make is the reason why it AGPL exists. When you take the course, the course is not distributed to you. You experience it. So legally speaking, you are not entitled to get the source, the modified source of the course, except if it's AGPL. So, of course, a MOOC is actually a very good instance of why the AGPL was created in the first place.

S2: 00:58:09.372 Yeah, but there is no equivalent of AGPL.

S5: 00:58:11.432 [crosstalk]-- but is it called ROA. No, that's [inaudible].

S2: 00:58:16.199 Well, we can raise an issue in the MOOC about that.

S6: 00:58:20.258 Yeah, good idea. Good idea.

S4: 00:58:20.812 The ROA is off topic.

S5: 00:58:22.115 I think the legal purpose--

S6: 00:58:22.758 [crosstalk].

S5: 00:58:24.417 --for a class, of course, teaching material and all the other kinds is a bit different too. So let's not discuss about those things because we are not lawyers, okay, or maybe I misunderstood someone's diploma, but-- no, that's interesting to observe drinking your own champagne and so on, but--

S2: 00:58:54.779 Let's come back to the contents of the course and not the matter.

S5: 00:58:57.485 Yes, yes, yes, and in the end, Marc is going to do whatever he wants. And the lawyers that we'll employ here will agree because I don't get any clue about all that. So we are safe, I hope.

S2: 00:59:17.136 No, we can--

S1: 00:59:17.423 Yeah, so the-- so the website you were pointing to Marc many times is choosealicense.com?

S2: 00:59:30.140 Yeah, but that's for new projects.

S5: 00:59:32.790 That's GitHub's. Nope, I think it was made by GitHub initially, but anyway, it's on a different subdomain and maybe a different project, anyway.

S2: 00:59:47.140 Oh, yeah, it's great. Yeah, but probably big.

S5: 00:59:49.541 That's quite interesting. This idea of having a wizard that asks you questions, what are your motives and what is your business model and what do you want to preserve and so on, which was, in my knowledge, initially invented for Creative Commons by the [Segan?] and Aaron Schwartz, probably or others because it's a nice way to explain that what is written in the legalities and in all the senses and so on is initially met, respond to motivations of the authors or the employee of the authors and so on, and so on. So maybe we could try and see if we can invent an activity a bit like what Loïc described, like explain to me one sentence and so on trying to search a wizard, "What would be the question to ask if we want to have this license text?" Then say we choose, we give you the answer and you have to guess what the questions are. Something like that is more closer to a multiple-choice activity that we can consider phonetics and so on. I don't know if that's clear.

S1: 01:01:17.799 Yes.

S3: 01:01:20.215 I'm not 100% sure I understood. Sorry.

S5: 01:01:23.398 I don't know if you're familiar with the Creative Common's dialogues where you choose if you want a non-commercial or commercial, identical or [crosstalk].

S3: 01:01:35.621 [Individual?].

S5: 01:01:37.394 So that's the same pattern for Choose a License where you say, "Do you want to make a business out of it? Do you want to preserve or renew? Do you want people to contribute back and so on?" So you have a flowchart that lets you get the best license given--

S2: 01:01:57.731 Goals.

S5: 01:01:58.360 --your context.

S3: 01:01:59.125 Yeah, so your idea of an activity is recreating--

S5: 01:02:03.158 So the--

S3: 01:02:03.568 --the questions that could be asked in this [crosstalk]?

S5: 01:02:06.706 Yeah, so that idea is I give you a kind of scenario, some kind of role play, something like that. And given this license, what could be the questions you ask. This and this license, what could be the question that could orientate the choice between one or the other, something like that. But maybe that's too complex.

S3: 01:02:30.348 So maybe they can even come up with a case. They can invent a fake project, for example, that they can describe in several words that would fit into this to make it a little bit more in the storytelling.

S4: 01:02:44.866 And what they could do, actually, it could be fun, is for the project they chose, they go through the process of choosing the license from what they understand of the project goals. And see if the license they come up with is the same or not.

S1: 01:03:04.471 So we extract some text about the project. We put it in the MOOC and this is the title of the project description, the philosophy of the company or the community, organization behind the project, what do you think. This is how they have an economical model like this. What do you think their license is.

S6: 01:03:34.752 And then--

S4: 01:03:34.307 Or even in a way that is very measurable is the outcome of the exercise is first, the URL of the license of the project, then screen captures of your choices in Choose a License, and in the end, the screenshot of the license you got. And you have 100 points if you come up with the exact same license as the project. Otherwise, you lost, but you learned something anyway.

S6: 01:04:17.576 Well, I don't know if that would be fair because again, we have those philosophical differences in terms of how you interpret the constraints that the project has of that company has and etc. And so what--

S4: 01:04:30.180 [crosstalk] you try to put yourself in the shoes of the project so it's not your choice. You're trying to say, "Okay, I'm the project lead." So you do your best to understand to put yourself in the shoes of the project.

S6: 01:04:45.957 Yeah, but I still think people will end up disagreeing--

S5: 01:04:47.143 [crosstalk] possible.

S6: 01:04:50.809 I think people still end up disagreeing on philosophical choice. And instead of this being an issue, I think this could be something interesting to show to say that, "Actually, you have rational thoughts, but you have also philosophical outlook way of looking at things that differ between people." And I think one way to show that would be on those use case. Instead of saying there is one answer where we are definitely going to get crucified by anyone who disagree with that choice, instead, maybe showing the statistics of what people think should be the license of that project and put that next to what the project thought. And there might be some interesting differences, like 90% thought it should be AGPL and interestingly, it's another proprietary license or whatever. And I think that is actually something interesting.

S2: 01:05:49.588 And we do course on edX.

S3: 01:05:50.247 [crosstalk].

S5: 01:05:52.075 Do you guys have--

S6: 01:05:54.282 Yup, you have that.

S5: 01:05:55.900 --[inaudible] enough to describe it in the pad because it's a bit difficult to explain maybe.

S1: 01:06:03.357 So that's why we will do transcripts of the videos and have textual--

S5: 01:06:07.503 Good luck because I'm lost. But it's a bit late and I'm probably too tired.

S3: 01:06:16.203 And most of [crosstalk] one--

S6: 01:06:16.429 Actually, for--

S3: 01:06:18.127 --sorry.

S6: 01:06:19.708 Go ahead.

S3: 01:06:20.598 Yeah, no, I just was thinking about one exercise I had once in a entrepreneurship class that was a pretty fun thing to do. I don't know if you're familiar with the Business Model Canvas. It's kind of a strategy, decision-making thing for businesses. It's where, basically, a business is broken down into the clients, the strategy, etc., etc. And we had to guess a company by reading the Business Model Canvas. So basically, you look at the description of the strategy of a project and you guess which one it is. So maybe something like this can be used for - I don't know - either seeing how a company came up with a license or even for guessing a license. Kind of a backward decision-making process. I don't know if that's a bit clear. The idea was--

S5: 01:07:13.425 It could be among all the examples that we have taken during the different resources that we have given them and so on. Did they listened to or were we clear enough so that they recognize the right projects, the right companies, the right communities. So you mentioned business models. So we didn't discuss about business models yet today, is this something we have time for, or is it too much?

S1: 01:07:52.157 Sure, because I'm not an expert. And I would like to know just a little bit more. I know that, Xavier, you have a business model for your company that also produce open FLOSS projects. As we said, Anna, our business model also could be as complex as a bigger strategy, or stuff.

S2: 01:08:15.798 We can talk about the different main business models around open source which are basically, relicensing when you ask for CLAs and you can relicense under a specific license if you pay, launch services like Red Hat's where you can pay to have support or services or development around products. SaaS, software as a service, for instance-- I don't know, [crosstalk]--

S1: 01:08:54.783 Google.

S2: 01:08:55.870 Yes, [inaudible]. I mean, many web-based free software project where you can pay to use the service hosted not by you but by whoever made it. WordPress, probably, is the best example.

S4: 01:09:17.520 No.

S2: 01:09:18.098 No?

S4: 01:09:18.906 Not good. [laughter]

S2: 01:09:20.269 Not good. Okay.

S4: 01:09:21.715 Right, because what they provide is not free software.

S2: 01:09:26.288 Okay, sorry.

S4: 01:09:26.987 Discourse.

S2: 01:09:28.620 Discourse, maybe.

S4: 01:09:28.418 Discourse is a good one. WebLink is another.

S6: 01:09:33.248 Although, I know why you're saying that Loïc, but I don't think we'll be able to simply say everything that doesn't correspond to the perceived definition of free software shouldn't be talked about or considered within the scope of this course.

S4: 01:09:51.068 So, I mean, wordpress.com is not free software at all.

S6: 01:09:55.668 Yes, but I think you would say the same thing of GitLab, for example, right?

S2: 01:10:00.382 Of course. Of course.

S4: 01:10:00.583 No, no, it's not running wordpress.org. It's completely different.

S2: 01:10:07.276 What?

S6: 01:10:07.760 Oh, yeah, it's true. I've heard that, actually, from a guy who was working there. But one thing that I wanted to say about this is that to read the different business model, there is one eye-level concept that I find really useful is the definition of scarcity because often, the business models will revolve around that concept either-- or whether it's creating artificial scarcity. So a proprietary license will be a good example of that. Like you are not allowed to distribute that. So if you want to be allowed to do that, you need to pay me something. On the other side, there will be things that are not artificial in terms of scarcity. So services like the business model Reddit or even the one from OpenCraft or some form will be intermediary in terms of, for example, dual licensing. You can actually use it any way you want, but if you want to be able to put your own conditions, then you need to pay. So it's not actually something scarce if you play by the rules, but you can have a scarcity in terms of licensing terms which is a bit artificial. So I think that is a useful way to look at business model because it tends to classify them quite [uniquely?] between things that flew kind of the fact that electronic goods by sense are not scarce, and some licenses just try to hold on making them scarce anyway so that they can keep their business models. And some others will try to embrace the fact that the digital aspect is not scarce like what we do for this MOOC, for example. The fact that the content of the course is freely distributable but if you want service, if you want mentorship, if you want things that can be multiplied to infinity, then there is still a need to divide those resources. And then having money involved is actually relevant.

S5: 01:12:21.175 Xavier, is this concept of scarcity the same as rivality of goods, no?

S6: 01:12:28.677 That's a good question.

S2: 01:12:29.607 Yes, it's very similar.

S6: 01:12:32.189 Yeah, yeah, we are using "Biens rivaux" in french. That's what you're saying. In that case, I think so. I would need to look at that more carefully but, yeah, probably.

S5: 01:12:41.090 I'm not completely into business models in the traditional or theoretical aspects and so on. I guess, there are quite some evolutions in the landscape in comparison to what happened until 10 years ago. I mean, between 20 years ago and 10 years ago, lots of things were quite stable.

S2: 01:13:08.787 Stable.

S5: 01:13:10.381 And then recently, there were other models like OpenCores or whatever, which are around free software but not exactly free software and so on where business models are trying to challenge those rules and so on. And probably revisiting stuff that happened 20 years ago because those new younger people don't respect the old ones like Loïc and me. And so should we venture into those conflicts and maybe not stabilize the new models, or should we stick to the old classical examples like what was written 20 years ago by the promoters of the OSI and all that?

S2: 01:13:58.744 So are you thinking about things like [code of?] cycle and [inaudible] spear?

S5: 01:14:06.060 I don't know because I'm not so much paying attention to those details. It's already hard enough to teach the basics that I understood 15 years ago. And I didn't have to learn about new stuff. But maybe you have guys because you have different [crosstalk].

S4: 01:14:25.733 What we absolutely need to convey in the course is to the extent that it matters to the contributor. So the business model from the point of view of the contributor is twofold. It's the business model of the project and their own business model. So going back to the situation of Xavier, he has a business model, Open edX has another and they interact. So that's where maybe we can spare the global theory about the possible economic models of someone running a project, but the course must articulate quite clearly what the interaction between business models imply for contribution. So Xavier would have stories to tell as a nonprofit contributor to a for-profit project. I would have other stories to tell. Independent contractors who contributed to Red Hat projects would also have interesting stories to tell, etc. So the fact that we are talking about contribution makes it a little bit more difficult to talk about business models, but it also simplifies the problem. That is if a business model has absolutely zero impact on contribution, then let's not talk about it. We don't care much.

S2: 01:16:21.158 Yeah, but then there is the issue of CLAs which do impact your contribution. If you're signing away you're right, then it's susceptible to a relicensing that deprived you of rights. And that's what happened with Elastic, for instance. And you have to-- so it's also an interesting case, but it's very advanced case study about the Qt case with the free Qt Foundation and the companies that makes Qt. And they have sort of a partnership where-- it's very hard to explain. And we probably have no time for that, but it's an interesting case study, I think.

S5: 01:17:15.567 So just to continue on what Loïc mentioned, should we have a first general description vital business model, classical duality between commerce, nonprofit, whatever, and so on without digging too much into the details? And then given that is already understood well, the point of view of the contributor where we specifically address what you should pay attention to like CLA-- I don't know what. And there will be the activities because that's the core.

S2: 01:18:02.517 Yes, I think that would be fine. And--

S4: 01:18:08.759 And again, [crosstalk]--

S2: 01:18:09.413 We can even talk about advanced topic then after that but--

S4: 01:18:13.302 Yeah, in the context of OpenStack, most of the participants came from companies. They were employees of companies who had financial interests in contributing to OpenStack. And they had to understand how the business model of their own company interacted with the business model of OpenStack as a foundation but also all the other stakeholders in OpenStack and who were the biggest influencers of OpenStack to figure out if their contribution had a chance to make it because it highly depended on how influential their companies was to the company who was the lead of the project at the time. So I think a good exercise is, "Okay, assert your own situation, your own business model if you have one, and for the contribution that you envision to make, how does it interact?"

S5: 01:19:26.413 So that's when we have maybe to deal with motivations of the contributors as human beings, as developers, as people. And the other aspects that we mentioned earlier, like companies, projects, communities and so on because you can have a friendly community where are you're happy to work with other people but unfortunately, your work is not valued because your email address is wrong if I'm just going to describe it.

S4: 01:19:57.236 Exactly. And you also have-- and it's very frequent that you have community managers that are paid by companies, they are very friendly, and you will get nowhere and you don't understand why. Why your contribution is overlooked, why does it take one year.

S5: 01:20:15.896 And then--

S4: 01:20:17.500 And it's not because--

S5: 01:20:17.652 And then if--

S4: 01:20:18.756 --you have a wrong email. It's just you don't fit in the business model.

S5: 01:20:24.972 And then if you can't talk because you signed something that prevents you from doing so and if there is no critical matter to move, it's a bad situation. So what you're illustrating, Loïc, is that beyond the first and all the happy faces of, "Well, let's do opensource. Yeah," there are dark sides. So how many horror stories should we tell in the MOOC?

S2: 01:21:07.065 Since open source is a successful term, there are many companies recently that just surf on this vocabulary to attract contributors that work for free for them, basically. And with CLAs and with the idea that if this model is at some point not going to be profitable, they can just take away the work that others created for free and just sell it.

S1: 01:21:37.509 We should talk about that in the MOOC.

S2: 01:21:43.514 By the way, it's already 8:00 PM, so.

S1: 01:21:45.593 Yeah, so that will be 8:00 past three.

S2: 01:21:50.153 So we should conclude or--?

S1: 01:21:53.542 Yes.

S2: 01:21:56.432 I think we talked about most of the points that we wanted to. Yeah, maybe one thing we haven't mentioned--

S5: 01:22:04.261 So did you plan to discuss psychology and so on, like mental health of contributors and stuff like that? Because thinking about the last discussion, I think there is plenty of stuff for Loïc counseling you on what not to do if you want to stay safe and so on.

S2: 01:22:31.767 That's for next week.

S5: 01:22:34.097 Okay, good. That's good.

S2: 01:22:35.705 That's exactly the topic of next week. When is the next-- I think it's Monday talk. Let me check.

S3: 01:22:47.354 Yeah, I also have to--

S2: 01:22:48.016 So that's a--

S3: 01:22:48.861 Sorry. You are saying?

S2: 01:22:51.900 No, that's the topic of next week. So it's a perfect transition. But you can ask her questions.

S3: 01:22:56.956 Yeah, the question regarding the last topic that was discussed is actually if there is a big chance for the learners of the MOOC to be kind of ignored and not managing to make a [comment?]. In terms of evaluation and the pedagogical activities, how can we deal with that situation if a person spends, I don't know, several hours trying to do something and they don't reach an answer?

S4: 01:23:23.364 So I thought about it a lot before starting the course because it's obviously a blocker. If you start a course saying you will get your commit in the project, I promise you, and you don't deliver, that's a killer. So what is paramount there is to properly choose the project and the contribution. Unfortunately, it's super easy to do that beforehand. What I always did with all the students is before the course, I talk to them and ask them to pick a contribution in the project that you want. Any project. And so this is great because it sounds like magic like you have a magician in front of you and say, "Pick any card in the deck and I will find it." And so most of the time, they find the contribution and the project-- the project is right most of the time. But the contribution most of the time is not right. It's too difficult, it's too old, it will not receive attention, etc., etc. And so it took me between 15 minutes to 1 hour for every student before the course. So it's not anything, but it's not very long to actually pick a contribution that matches their skill and that is going to be merged because it has all the qualities of a contribution that will be merged. It has been recently created, it matches a need that someone cares about. Now, this someone has the power to merge it. And you find that it may not be labeled easy contribution, but still, it's not very difficult. And then you can do the magic trick. So then you escape the problem that you describe, which is what if the contribution does not make it because people don't care, because you choose a contribution where you know that at least one person cares.

S2: 01:25:50.620 In my opinion, this work can be offloaded to subprojects. If the project has an active community, then that's a topic of next week. That's also a transition. Then getting in touch with the project and actually discussing what you plan to do or what can be done or what is easy to do in the project is a way to find things that interest the project and can interest you. And discussing will yield usually tasks that people can do. By interacting, you will, basically, give information about what you are able to do, what are your skills, and you will get in return from people from the community ideas about what they plan to do. And matching those can be done not from the MOOC but from the project. But--

S1: 01:26:46.856 And that is a project with a community managers that doesn't want you to-- or drags you the wrong way as you explained, Loic.

S5: 01:26:57.709 Yeah, just to address the concern that Anna raised-- though, I think it would be quite important to have a questionnaire at the beginning of the course. It is well calibrated so that we force people to make clear what their expectations are so that they are not deceived because we will tell them that it's normal, that it takes time, and they can't be sure that people are available and so on and so forth. But then, they are not too disappointed because, "Okay, that's just a learning process. That's not a guarantee and so on. And we are no magicians." So it means that we have to test to benchmark the questionnaire at the beginning so that people dedicate enough effort on it. It's not just something easy. And you do it because you know that you will be--

S2: 01:27:59.470 You will learn in the process.

S5: 01:28:01.349 Yeah, that's important. Or you may revise simple tasks or questionnaires. Something like that. So it's not just a questionnaire and edits that we get rid of. It's something that is a kind of portfolio that along your travel, you will revise your expectations. Something like that.

S1: 01:28:22.912 All right. I think it's time to conclude now.

S2: 01:28:27.163 So on Monday, we will talk about community management and discussion mechanisms and code of conduct and diversity in the world of open source and diversity of contributors in the world of open source and how this kind of issues evolved with some activities like actually getting in touch with the project you want to contribute to if you haven't already which covers the last several points we've discussed, I think.

S3: 01:29:02.605 Great transition.

S2: 01:29:05.795 So see you on Monday, I guess?

S1: 01:29:09.152 I hope. And thank you very much to all.

S6: 01:29:10.363 See you on Monday.

S1: 01:29:12.354 And to the new participants.

S3: 01:29:12.866 Thank you.

S1: 01:29:13.921 Thank you again.

S2: 01:29:14.134 Thanks.

S4: 01:29:16.071 Have a great.
